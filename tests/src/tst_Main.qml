import QtQuick 2.7
import QtTest 1.0
import "../../src/Main"

Item {
    width: 800; height: 600


    Main {
        id: mainWindow

        Component.onCompleted: {
            mainWindow.visible = true
            var startWindow = findChild(mainWindow,"startWindow")
            startWindow.visible = false
            var guideDialog = findChild(findChild(findChild(mainWindow,"guideDialog"),"guideDialogContentItem"),"guideDialogCancelButton")
            mouseClick(guideDialog,5,5,Qt.LeftButton)
        }
    }

    TestCase {
        name: "Main"; when: windowShown

        function test_OpenCloseContextMenu() {



            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)

            var visibleContextMenu = findChild(mainWindow,"contextMenu").visible

            //mouseClick(ism);
            //var widthAfterClick = ism.width;
            compare(true,visibleContextMenu);

            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.LeftButton)
            visibleContextMenu = findChild(mainWindow,"contextMenu").visible

            compare(false, visibleContextMenu);
        }

        function test_OpenCloseCommandWindowFromContextMenu() {



            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)

            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,5, 5,Qt.LeftButton)


            compare(mainWindow.visibleGeneralWindow,true);
            //var charactersWindowComponent = findChild(mainWindow,"commandWindowLoader").sourceComponent
            var commandsWindow = findChild(mainWindow,"stackView").currentItem
            //compare(findChild(charactersWindowComponent,"charactersWindow").table, "imagesList")
            //console.debug(">>>",commandsWindow.currentItem)
            compare("commandsList", commandsWindow.table)

        }


        function test_CommandWindow() {

            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)

            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,5,5,Qt.LeftButton)

            var commandWindow = findChild(mainWindow,"stackView").currentItem

            var commandName = findChild(commandWindow,"nameEdit")
            var command = findChild(commandWindow,"commandEdit")
            console.debug(">>>",command)

            var bar = findChild(commandWindow,"bar")
            var voice = findChild(commandWindow,"voiceInputEdit")

            // проверка механизма задвигания/отодвигания строки ввода
            compare("hide", findChild(commandWindow,"inputRect").state)


            //mousePress(,10,10,Qt.LeftButton)
            findChild(commandWindow,"newButton").mouseAreaClicked()

            compare("show", findChild(commandWindow,"inputRect").state)



            var commandTable = findChild(commandWindow,"grid")
            var gridview = findChild(commandTable,"gridCommandTable")


            // проверка добавления элемента с пустыми строками
            var count = gridview.count
            // charactersTable.addingImage()
            mouseClick(findChild(commandWindow,"addButton"),0,0,Qt.LeftButton)
            verify(count === gridview.count, "Element shouldn't be added")



            // проверка добавления элемента с заполнением строк
            mouseClick(commandName,4,4,Qt.LeftButton)
            commandName.text  = "asdf"//Math.floor(Math.random() * Math.floor(10000)).toString()
            /*var commandNameText = "asdf"//Math.floor(Math.random() * Math.floor(10000)).toString()
            for (var i = 0; i < commandNameText.length;i++)
            {
                keyPress(commandNameText[i])
            }
            console.debug(commandName.text)*/

            //tag.text = Math.floor(Math.random() * Math.floor(10000)).toString()
            mouseClick(command,5,5,Qt.LeftButton)
            commandWindow.pathText = "subl"
            //command.text = "subl"
            /*var commandText = "subl"
            for (var j = 0; j < commandText.length;j++)
            {
                keyPress(commandText[j])
            }
            */
            mouseClick(voice,5,5,Qt.LeftButton)
            voice.text = "ква"
            /*
            var voiceText = "ква"
            for (var k = 0; k < voiceText.length;k++)
            {
                keyPress(voiceText[k])
            }*/
            bar.setCurrentIndex(1)
            //commandWindow.pathText = "subl"
            //commandWindow.rezultText = "привет мир"

            // path.text = "file:///home/alner/pigeon.png"
            count = gridview.count

            console.log(commandName.text, command.text,voice.text, count, gridview.count)
            commandTable.addingCommand()
            //mouseClick(findChild(findChild(findChild(charactersWindow,"rowInputCenterBtns"),"addButton"),"stylishArea"),0,0,Qt.LeftButton)
            //findChild(commandWindow,"addButton").mouseAreaClicked()
            console.log(gridview.count)
            verify(count !== gridview.count, "Element should be added")


            // проверка удаления элемента
            count = gridview.count
            //mouseClick(commandTable,10,10,Qt.LeftButton)
            gridview.currentIndex = 0
            commandTable.removeRow()
            //mouseClick(findChild(findChild(findChild(charactersWindow,"rowBtns"),"deleteButton"),"stylishArea"),10,10,Qt.LeftButton)

            verify(count !== gridview.count, "Element should be deleted")

        }

        /*
        function test_OpenCloseCharactersWindowFromContextMenu() {

            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)

            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,0,0,Qt.LeftButton)

            compare(mainWindow.visibleImageSelectWindow,true);
            var charactersWindowComponent = findChild(mainWindow,"charactersWindowLoader").sourceComponent
            var charactersWindow = findChild(findChild(mainWindow,"charactersWindowLoader"),"charactersWindow")
            //compare(findChild(charactersWindowComponent,"charactersWindow").table, "imagesList")
            compare("imagesList", charactersWindow.table)

        }
        function test_CharactersWindow() {

            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)

            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,0,0,Qt.LeftButton)

            compare(mainWindow.visibleImageSelectWindow,true);
            var charactersWindowComponent = findChild(mainWindow,"charactersWindowLoader").sourceComponent
            var charactersWindow = findChild(findChild(mainWindow,"charactersWindowLoader"),"charactersWindow")
            //compare(findChild(charactersWindowComponent,"charactersWindow").table, "imagesList")
            compare("imagesList", charactersWindow.table)

            var tag = findChild(charactersWindow,"tag")
            var path = findChild(findChild(charactersWindow,"pathTextRow"),"pathText")

            // проверка механизма задвигания/отодвигания строки ввода
            compare("hide", tag.state)

            charactersWindow.changeAddingVisible()

            compare("show", tag.state)

            mouseClick(findChild(findChild(charactersWindow,"rowInputCenterBtns"),"cancelButton"),0,0,Qt.LeftButton)

           // compare("hide", tag.state)

            var charactersTable = findChild(charactersWindow,"charactersTable")
            var gridview = findChild(findChild(charactersTable,"mouseAreaGridView"),"gridView")

            // проверка добавления элемента с пустыми строками
            var count = gridview.count
            // charactersTable.addingImage()
            mouseClick(findChild(findChild(charactersWindow,"rowInputCenterBtns"),"addButton"),0,0,Qt.LeftButton)
            verify(count === gridview.count, "Element shouldn't be added")


            // проверка добавления элемента с заполнением строк
            mouseClick(tag,0,0,Qt.LeftButton)
            var tagText = Math.floor(Math.random() * Math.floor(10000)).toString()
            tagText += Math.floor(Math.random() * Math.floor(10000)).toString()
            for (var i = 0; i < tagText.length;i++)
            {
                keyPress(tagText[i])
            }

            //tag.text = Math.floor(Math.random() * Math.floor(10000)).toString()
            mouseClick(path,5,5,Qt.LeftButton)
            var pathText = "file:///home/alner/pigeon.png"
            for (var j = 0; j < pathText.length;j++)
            {
                keyPress(pathText[j])
            }

            // path.text = "file:///home/alner/pigeon.png"
            count = gridview.count

            console.log(tag.text, path.text,count, gridview.count)
            charactersTable.addingImage()
            //mouseClick(findChild(findChild(findChild(charactersWindow,"rowInputCenterBtns"),"addButton"),"stylishArea"),0,0,Qt.LeftButton)

            console.log(charactersWindow.errName)
            verify(count !== gridview.count, "Element should be added")

            // проверка удаления элемента
            count = gridview.count
            charactersTable.removeImage(gridview.count - 1)
            //mouseClick(findChild(findChild(findChild(charactersWindow,"rowBtns"),"deleteButton"),"stylishArea"),10,10,Qt.LeftButton)

            verify(count !== gridview.count, "Element should be deleted")

        }

        function test_OpenCloseRemindersWindowFromContextMenu() {

            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)

            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,0,cm.height / 5 * 2 + 5,Qt.LeftButton)

            compare(mainWindow.visibleReminderWindow,true);
            //var charactersWindowComponent = findChild(mainWindow,"commandWindowLoader").sourceComponent
            var reminderWindow = findChild(findChild(mainWindow,"reminderWindowLoader"),"reminderWindow")
            //compare(findChild(charactersWindowComponent,"charactersWindow").table, "imagesList")
            compare("remindersList", reminderWindow.table)

        }

        function test_AddingInRemindersTable(){

            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)
            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,0,cm.height / 5 * 2 + 5,Qt.LeftButton)
            var rw = findChild(findChild(mainWindow,"reminderWindowLoader"),"reminderWindow")


            var addButton = findChild(rw, "adding_reminder");
            var remTable = findChild(rw, "reminders_table");
            var count1 = remTable.count

            addButton.mouseAreaClicked();

            var count2 = remTable.count
            console.log(count1)
            console.log(count2)
            verify(count1 + 1 === count2)
        }

        function test_SavingInRemindersTable(){
            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)
            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,0,cm.height / 5 * 2 + 5,Qt.LeftButton)
            var rw = findChild(findChild(mainWindow,"reminderWindowLoader"),"reminderWindow")


            var addButton = findChild(rw, "adding_reminder");
            var saveButton = findChild(rw, "saving_reminder");
            var remTable = findChild(rw, "reminders_table");
            var enabledValue1 = remTable.enabled_value;

            //addButton.mouseAreaClicked();
            var date_plase = findChild(remTable, "datePlase")
            date_plase.text = "2020.05.15"
            //var task_text = findChild(remTable, "taskText")

            saveButton.mouseAreaClicked();

            var enabledValue2 = remTable.enabled_value;

            console.log(enabledValue1)
            console.log(enabledValue2)

            verify(enabledValue1 === enabledValue2)
        }

        function test_DeletingInRemindersTable(){

            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)
            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,0,cm.height / 5 * 2 + 5,Qt.LeftButton)
            var rw = findChild(findChild(mainWindow,"reminderWindowLoader"),"reminderWindow")

            var deleteButton = findChild(rw, "deleting_reminder");
            var addButton = findChild(rw, "adding_reminder");
            var remTable = findChild(rw, "reminders_table");
            var count1 = remTable.count

            addButton.mouseAreaClicked();
            remTable.currentIndex = 1
            deleteButton.mouseAreaClicked();

            var count2 = remTable.count
            console.log(count1)
            console.log(count2)
            verify(count1 === count2)
        }

        function test_DateAddingInRemindersTable(){

            mouseClick(mainWindow,mainWindow.width / 2,mainWindow.height / 2,Qt.RightButton)
            var cm = findChild(mainWindow,"contextMenu")
            mouseClick(cm,0,cm.height / 5 * 2 + 5,Qt.LeftButton)
            var rw = findChild(findChild(mainWindow,"reminderWindowLoader"),"reminderWindow")


            var dateTable = findChild(rw, "date_table")
            var saveButton = findChild(rw, "saving_reminder")
            var count1 = dateTable.count
            saveButton.mouseAreaClicked();
            var count2 = dateTable.count
            verify(count1 === count2)
        }
        */
    }

    /*
    TestCase {
        name: "ImageSelectionMenu"; when: windowShown

        function test_CharactersWindow() {

            var tag = findChild(ism,"tag")
            ism.changeAddingVisible()

            compare("show", tag.state)

            //keySequence("Esc")

           // compare("hide", tag.state)

        }
    }
*/

}


