
QT += multimedia sql quick

CONFIG += c++11
CONFIG += warn_off qmltestcase

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

include(../src/classes/qxtglobalshortcut5/qxt.pri)

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        ../src/classes/levenshtein.cpp \
        ../src/classes/database.cpp \
        ../src/classes/settings.cpp \
        ../src/classes/speechtotext.cpp \
        ../src/classes/action.cpp \
        src/tst_Main.cpp

RESOURCES += src/tst_Main.qml \
             ../src/qrc/qml.qrc \
             ../src/qrc/fonts.qrc \
             ../src/qrc/images.qrc \
             ../src/qrc/sounds.qrc

HEADERS += \
    ../include/levenshtein.h \
    ../include/database.h \
    ../include/settings.h \
    ../include/action.h \
    ../include/speechtotext.h \
    include/Setup.h

TRANSLATIONS += ../translations/QtLanguage_ru.ts



