#ifndef SETUP_H
#define SETUP_H

#include <QtQuickTest>
#include <QObject>
#include <QQmlEngine>
#include <QQmlContext>
#include "../include/database.h"
#include "../include/settings.h"
#include "../include/speechtotext.h"

//#include "ImageSelectionMenu.qml"

class Setup : public QObject
{
    Q_OBJECT

public:
    Setup() {

        QSurfaceFormat format;
            format.setDepthBufferSize(32);
            format.setRedBufferSize (10);
            format.setGreenBufferSize (10);
            format.setBlueBufferSize (10);
            format.setAlphaBufferSize(0);
            format.setRenderableType(QSurfaceFormat::OpenGL);
            format.setVersion(4, 6);
            QSurfaceFormat::setDefaultFormat(format);
    }
    ~Setup() {
        delete db;
        delete settings;
        delete stt;
    }
public slots:
    void qmlEngineAvailable(QQmlEngine *engine)
    {
        db = new Database();

        if (!db->isOpen())
        {
            qDebug() << QCoreApplication::tr("Failed to open the database. Shutdown.");
            return;
        }

        if (!db->createDatabaseTables())
        {
            qDebug() << QCoreApplication::tr("Failed to create database. Shutdown.");
            return;
        }
        engine->rootContext()->setContextProperty("db", db);

        settings = new Settings();
        settings->loadSettings();
        engine->rootContext()->setContextProperty("settings", settings);

        stt = new SpeechToText(settings->getAppActivation(),settings->getFastCommandsActivation());
        stt->setGlobalShortcuts(db->receiveShortcuts());
        engine->rootContext()->setContextProperty("stt", stt);
    }
private:
    Database* db;
    Settings* settings;
    SpeechToText* stt;
};

#endif // SETUP_H
