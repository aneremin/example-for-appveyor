/**
  * @file
  * @brief Пользовательский title-bar
  *
  * Прямоугольник с изображением иконки приложения и кнопок
  * свернуть/развернуть/выйти
*/

import QtQuick 2.0
import QtQuick 2.14
import QtQuick.Controls 2.14

/*!
    \class CustomSystemPanel
    \brief Прямоугольнк - верхняя панель приложения
 */
Rectangle{

    height: 30
    color: "#30313A"
    //gradient: style.getBackgroundGradient()//style.getGradientStandart()
    z:3
    /** @brief инентификатор окна, к которому присоединяется эта верхняя панель*/
    property var window_id
    /** @brief Значение X перед перетаскиванием*/
    property int previousX
    /** @brief Значение Y перед перетаскиванием*/
    property int previousY

    MouseArea {
        anchors.fill: parent

        onPressed: {
            previousX = mouseX
            previousY = mouseY
        }
        onDoubleClicked: {

            if (window_id.visibility === 4)
            {
                window_id.showNormal()
            }
            else
                window_id.showMaximized()
        }

        onMouseXChanged: {
            var dx = mouseX - previousX
            window_id.setX(window_id.x + dx)
        }

        onMouseYChanged: {
            var dy = mouseY - previousY
            window_id.setY(window_id.y + dy)
        }
    }

    Rectangle{
        id: close_bt
        anchors.right: parent.right
        height: 30
        width: 30
        color: "#30313A"//gradient: style.getGradientStandart()
        Image {
            anchors.fill: parent
            anchors.topMargin: 8
            anchors.bottomMargin: 8
            anchors.leftMargin: 8
            anchors.rightMargin: 8
            id: close_img
            source: "/resources/img/close.png"
        }
        states:[
            State {
                name: "BUTTON_ENTERED"
                PropertyChanges { target: close_bt; gradient: style.getAttentionGradientStandart()}
            },
            State {
                name: "BUTTON_EXITED"
                PropertyChanges { target: close_bt; color: "#30313A"}
            }
        ]
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: close_bt.state = "BUTTON_ENTERED"
            onExited:  close_bt.state = "BUTTON_EXITED"
            onClicked: window_id.close()
        }
    }
    Rectangle{
        id: expand_bt
        anchors.right: close_bt.left
        height: 30
        width: 30
        color: "#30313A"//gradient: style.getGradientStandart()
        Image {
            anchors.fill: parent
            anchors.topMargin: 5
            anchors.bottomMargin: 5
            anchors.leftMargin: 5
            anchors.rightMargin: 5
            id: expand_img
            source: "/resources/img/expand.png"
            property bool flipped: false
            transform: Rotation { // способ трансформации изображения
                id: rotation // поворот
                origin.x: 10; origin.y: 10
                axis {x: 1; y: 0; z:0} // как поворачиваем
                angle: 0 // угол
            }
            states: State { // задаем состояние
                name: "back" // название состояния
                PropertyChanges { target: rotation; angle: 180} // изменяемые параметры
                when: expand_img.flipped // когда переменная указывает на то, что был совершен переворот
            }

            transitions: Transition {
                NumberAnimation { target: rotation; property: "angle"; duration: 1000;} // задаем время анимации
            }
        }

        states:[
            State {
                name: "BUTTON_ENTERED"
                PropertyChanges { target: expand_bt; gradient: style.getGradientEntered()}
            },
            State {
                name: "BUTTON_EXITED"
                PropertyChanges { target: expand_bt; color: "#30313A"}
            }
        ]

        MouseArea {
            anchors.fill: parent
            property bool flag: false
            hoverEnabled: true
            onEntered: expand_bt.state = "BUTTON_ENTERED"
            onExited:  expand_bt.state = "BUTTON_EXITED"
            onClicked: {
                if (!flag){
                    window_id.showMaximized()
                    flag = true
                }
                else{
                    window_id.showNormal()
                    flag = false
                }
                expand_img.state = "back"
                expand_img.flipped = !expand_img.flipped
            }
        }
    }
    Rectangle{
        id: minimize_bt
        anchors.right: expand_bt.left
        height: 30
        width: 30
        color: "#30313A"//gradient: style.getGradientStandart()
        Image {
            anchors.fill: parent
            anchors.topMargin: 5
            anchors.bottomMargin: 5
            anchors.leftMargin: 5
            anchors.rightMargin: 5
            id: minimize_img
            source: "/resources/img/minimize.png"
        }
        states:[
            State {
                name: "BUTTON_ENTERED"
                PropertyChanges { target: minimize_bt; gradient: style.getGradientEntered()}
            },
            State {
                name: "BUTTON_EXITED"
                PropertyChanges { target: minimize_bt; color: "#30313A"}
            }
        ]
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: minimize_bt.state = "BUTTON_ENTERED"
            onExited:  minimize_bt.state = "BUTTON_EXITED"
            onClicked: window_id.showMinimized()
        }
    }
}
