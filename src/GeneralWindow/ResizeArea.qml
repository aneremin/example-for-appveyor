/**
  * @file
  * @brief Группа областей мыши для изменения положения и размера окна
  *
*/
import QtQuick 2.0

/*!
    \class ResizeArea
    \brief Группа областей мыши для изменения положения и размера окна
 */
Item {

    z:2
    /** @brief id окна для изменения его размеров */
    property var window_id
    /** @brief Координата X до перемещения курсора */
    property int previousX
    /** @brief Координата Y до перемещения курсора */
    property int previousY
    /** @brief Изменение положения мыши по оси Y */
    property var dy
    /** @brief Изменение положения мыши по оси X */
    property var dx

    /** @brief Функция изменения размера окна в направлении вверх */
    function top_area_resize(previousY, mouseY){
        dy = mouseY - previousY

        if ((window_id.height > window_id.minimumHeight) || (dy <= 0)){
            if (window_id.height - dy >= window_id.minimumHeight){
                window_id.setY(window_id.y + dy)
                window_id.setHeight(window_id.height - dy)
            }
            else{
                window_id.setY(window_id.y + (window_id.height - window_id.minimumHeight))
                window_id.height = window_id.minimumHeight
            }
        }
    }

    /** @brief Функция изменения размера окна в направлении вниз */
    function bottom_area_resize(previousY, mouseY){
        dy = mouseY - previousY
        if ((window_id.height > window_id.minimumHeight) || (dy >= 0)){
            if (window_id.height + dy >= window_id.minimumHeight){
                window_id.setHeight(window_id.height + dy/5)
            }
            else{
                window_id.height = window_id.minimumHeight
            }
        }
    }
    /** @brief Функция изменения размера окна в направлении влево */
    function left_area_resize(previousX, mouseX){
        dx = mouseX - previousX
        if ((window_id.width > window_id.minimumWidth) || (dx <= 0)){
            if (window_id.width - dx >= window_id.minimumWidth){
                window_id.setX(window_id.x + dx)
                window_id.setWidth(window_id.width - dx)
            }
            else{
                window_id.setX(window_id.x + (window_id.width - window_id.minimumWidth))
                window_id.width = window_id.minimumWidth
            }
        }
    }
    /** @brief Функция изменения размера окна в направлении вправо */
    function right_area_resize(previousX, mouseX){
        dx = mouseX - previousX
        console.log(dx)

        if ((window_id.width > window_id.minimumWidth) || (dx >= 0)){
            if (window_id.width + dx >= window_id.minimumWidth){
                window_id.setWidth(window_id.width + dx/5)
            }
            else{
                window_id.width = window_id.minimumWidth
            }
        }
    }

    MouseArea {
        id: topArea
        height: 5
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        cursorShape: Qt.SizeVerCursor

        onPressed: {
            previousY = mouseY
        }

        onMouseYChanged: {
            top_area_resize(previousY, mouseY)
        }
    }

    MouseArea {
        id: bottomArea
        height: 5
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        cursorShape: Qt.SizeVerCursor

        onPressed: {
            previousY = mouseY
        }

        onMouseYChanged: {
            bottom_area_resize(previousY, mouseY)
        }
    }

    MouseArea {
        id: leftArea
        width: 5
        anchors {
            top: topArea.bottom
            bottom: bottomArea.top
            left: parent.left
        }
        cursorShape: Qt.SizeHorCursor

        onPressed: {
            previousX = mouseX
        }

        onMouseXChanged: {
            left_area_resize(previousX, mouseX)
        }
    }

    MouseArea {
        id: rightArea
        width: 5
        anchors {
            top: topArea.bottom
            bottom: bottomArea.top
            right: parent.right
        }
        cursorShape:  Qt.SizeHorCursor

        onPressed: {
            previousX = mouseX
        }

        onMouseXChanged: {
            right_area_resize(previousX, mouseX)
        }
    }

    MouseArea {
        id: left_top_corner_area
        anchors.left: parent.left
        anchors.top: parent.top
        z:3
        width: 5
        height: 5

        cursorShape: Qt.SizeFDiagCursor
        onPressed: {
            previousX = mouseX
            previousY = mouseY
        }

        onMouseXChanged: {
            left_area_resize(previousX, mouseX)
            top_area_resize(previousY, mouseY)
        }
    }

    MouseArea {
        id: right_top_corner_area
        anchors.right: parent.right
        anchors.top: parent.top
        z:3
        width: 5
        height: 5

        cursorShape: Qt.SizeBDiagCursor
        onPressed: {
            previousX = mouseX
            previousY = mouseY
        }

        onMouseXChanged: {
            right_area_resize(previousX, mouseX)
            top_area_resize(previousY, mouseY)
        }
    }

    MouseArea {
        id: right_bottom_corner_area
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        z:3
        width: 5
        height: 5

        cursorShape: Qt.SizeFDiagCursor
        onPressed: {
            previousX = mouseX
            previousY = mouseY
        }

        onMouseXChanged: {
            right_area_resize(previousX, mouseX)
            bottom_area_resize(previousY, mouseY)
        }
    }

    MouseArea {
        id: left_bottom_corner_area
        anchors.right: parent.left
        anchors.bottom: parent.bottom
        z:3
        width: 5
        height: 5

        cursorShape: Qt.SizeBDiagCursor
        onPressed: {
            previousX = mouseX
            previousY = mouseY
        }

        onMouseXChanged: {
            left_area_resize(previousX, mouseX)
            bottom_area_resize(previousY, mouseY)
        }
    }
}
