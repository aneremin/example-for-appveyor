/**
  * @file
  * @brief Табличка с подсказками
  *
  * Прямоугольник с текстом внутри, используется для ознакомления пользователя
  * с программой
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0

/*!
    \class GuideRectangle
    \brief Прямоугольник с текстом внутри, используется для ознакомления пользователя
  * с программой
 */
Rectangle{
    id: guide_rect
    width: Screen.width / 10
    height: txt.lineCount == 1 ? txt.contentHeight * (txt.lineCount + 2) : txt.contentHeight * 1.5 + Screen.height / 36
    //anchors.fill: parent
    border.color: "transparent"
    border.width: 2
    radius: 20
    /** @brief Текст подсказки*/
    property string promptText

    /** @brief Сигнал о состоявшемся нажатии на кнопку*/
    signal promptClicked

    function doUpdate(show) {
        prompt.visible = show
    }
    /*MouseArea {
        anchors.fill: parent
        propagateComposedEvents: true
    }*/

    color: "transparent"


    Rectangle{
        id: background_rect
        //anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        gradient: Gradient{
            id: backgroundGradient
            GradientStop {
                position: 0
                color: "#3C4653"

                SequentialAnimation on color {
                    id: colorAnimation
                    running: true
                    loops: Animation.Infinite
                    ColorAnimation { to: "#493DA0" }
                    ColorAnimation { to: "#5C4653"; duration: 1500 }
                    ColorAnimation { to: "#773143"; duration: 1500 }
                    ColorAnimation { to: "#771E36"; duration: 1500 }
                    ColorAnimation { to: "#771E4D"; duration: 1500 }
                    ColorAnimation { to: "#770140"; duration: 1500 }
                    ColorAnimation { to: "#771E4D"; duration: 1500 }
                    ColorAnimation { to: "#771E36"; duration: 1500 }
                    ColorAnimation { to: "#773143"; duration: 1500 }
                    ColorAnimation { to: "#5C4653"; duration: 1500 }
                    ColorAnimation { to: "#493DA0"}
                }

            }
            GradientStop {
                position: 1
                color: "#30313A"
                SequentialAnimation on color {
                    id: colorAnimation2
                    running: true
                    loops: Animation.Infinite
                    ColorAnimation { to: "#30123A"}
                    ColorAnimation { to: "#301262"; duration: 1500 }
                    ColorAnimation { to: "#22264A"; duration: 1500 }
                    ColorAnimation { to: "#1B2050"; duration: 1500 }
                    ColorAnimation { to: "#4A2050"; duration: 1500 }
                    ColorAnimation { to: "#712050"; duration: 1500 }
                    ColorAnimation { to: "#4A2050"; duration: 1500 }
                    ColorAnimation { to: "#1B2050"; duration: 1500 }
                    ColorAnimation { to: "#22264A"; duration: 1500 }
                    ColorAnimation { to: "#301262"; duration: 1500 }
                    ColorAnimation { to: "#30123A";}
                }

            }
        }
        border.color: "white"
        border.width: 2
        radius: 20
        width: 0
        height: 0//parent.height

        SequentialAnimation on width {
            id: width_maximize
            running: false
            PropertyAnimation { to: guide_rect.width; duration: 800}
        }

        SequentialAnimation on height {
            id: height_maximize
            running: false

            PropertyAnimation { to: guide_rect.height; duration: 800}
        }

        SequentialAnimation on width {
            id: width_minimize
            running: false
            PropertyAnimation { to: guide_rect.width; duration: 1000}
            PropertyAnimation { to: 0; duration: 800}
        }

        SequentialAnimation on height {
            id: height_minimize
            running: false
            PropertyAnimation { to: guide_rect.height; duration: 1000}
            PropertyAnimation { to: 0; duration: 800}
        }
    }

    Text {
        id:txt
        anchors.margins: Screen.height / 72
        anchors.topMargin: Screen.height / 36
        anchors.fill: parent
        text: promptText
        color: "white"
        font.pixelSize: Screen.height / 72
        wrapMode: Text.WordWrap
        opacity: 0

        SequentialAnimation on opacity {
            id: text_opacity_inc
            running: false
            PropertyAnimation { to: 0; duration: 800}
            PropertyAnimation { to: 1; duration: 800}
        }

        SequentialAnimation on opacity {
            id: text_opacity_dec
            running: false
            PropertyAnimation { to: 1; duration: 200}
            PropertyAnimation { to: 0; duration: 800}
        }
    }

    Rectangle {
        id: closeBtn
        color: "transparent"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        height: 10
        width: height
        opacity: 0
        SequentialAnimation on opacity {
            id: closeBtn_opacity_inc
            running: false
            PropertyAnimation { to: 0; duration: 800}
            PropertyAnimation { to: 0.8; duration: 800}
        }
        SequentialAnimation on opacity {
            id: closeBtn_opacity_dec
            running: false
            PropertyAnimation { to: 0; duration: 300}
        }

        Image {
            anchors.fill: parent
            anchors.margins: 1

            id: close_img
            source: "/resources/img/close.png"
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onEntered: if (parent.opacity !== 0) parent.opacity = 1
            onExited: if (parent.opacity !== 0) parent.opacity = 0.8
            onClicked: {
                hideElements()
            }
        }
    }
    function showElements() {
        prompt.visible = false
        height_maximize.start()
        width_maximize.start()
        text_opacity_inc.start()
        closeBtn_opacity_inc.start()
    }

    function hideElements() {
        closeBtn_opacity_dec.start()
        text_opacity_dec.start()
        width_minimize.start()
        height_minimize.start()
    }

    property int promptRotation: 0

    Image {
        id: prompt
        rotation: promptRotation
        anchors.horizontalCenter: (promptRotation === 0) ? parent.horizontalCenter : undefined
        anchors.bottom: (promptRotation === 0) ? parent.bottom : undefined
        anchors.right: (promptRotation === -90) ? parent.right : undefined
        anchors.verticalCenter: (promptRotation === -90) ? parent.verticalCenter : undefined
        anchors.top: (promptRotation === 180) ? parent.top : undefined
        source: "/resources/img/prompt"
        height: 30
        width: 30
        MouseArea {
            anchors.fill: parent
            onClicked: {
                showElements()
                promptClicked()
            }
        }

        SequentialAnimation on anchors.bottomMargin {
            id: top_to_bottom_animation
            running: true
            loops: Animation.Infinite
            PropertyAnimation { to: 10; duration: 1000}
            PropertyAnimation { to: -10; duration: 1000}
        }
        SequentialAnimation on anchors.topMargin {
            id: bottom_to_top_animation
            running: true
            loops: Animation.Infinite
            PropertyAnimation { to: 0; duration: 1000}
            PropertyAnimation { to: 20; duration: 1000}
        }

        SequentialAnimation on anchors.rightMargin {
            id: left_to_right_animation
            running: true
            loops: Animation.Infinite
            PropertyAnimation { to: 10; duration: 1000}
            PropertyAnimation { to: -10; duration: 1000}
        }

        Connections {
            target: settings
            onPromptModeChanged: {
                if (closeBtn.opacity !== 0)
                    hideElements()
            }
        }

        /*SequentialAnimation on x {
           id: left_to_right_animation
           running: false
           loops: Animation.Infinite
           PropertyAnimation { to: prompt.x + 10; duration: 500}
           PropertyAnimation { to: prompt.x + 20; duration: 500}
           PropertyAnimation { to: prompt.x + 10; duration: 500}
           PropertyAnimation { to: prompt.x; duration: 500}
       }*/
    }
}

