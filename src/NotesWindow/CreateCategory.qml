/**
  * @file
  * @brief Грид категорий при создании заметки
  *
  * Грид со всеми возможными категориями
*/

import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0

/*!
    \class CreateCategory
    \brief Грид со всеми возможными категориями для добавления в заметку
 */
MouseArea {

    height: 30

    propagateComposedEvents: true

    GridView {
        id: catgrid

        anchors.fill: parent
        anchors.margins: 5
        //anchors.leftMargin: 10
        //anchors.rightMargin: 10
        height: parent.height
        clip: true
        cellWidth: cellHeight * 2.9
        cellHeight: 35
        //focus: true
        //interactive: true
        //flow: (horFlick) ? GridView.FlowTopToBottom : none
        //layoutDirection: Qt.LeftToRight
        //verticalLayoutDirection: Grid.TopToBottom
        snapMode: GridView.SnapToRow
        flickableDirection: Flickable.VerticalFlick

        delegate: component2Category
        model: listModelCreate
    }

    ListModel
    {
        id: listModelCreate
        Component.onCompleted: {

            var list = db.selectAllTuples(2, catsTable)

            var i = 1
            if (list[0] > 1)
            {
                for (; i < list[0];) {
                    append({cat: list[i], colorCat: list[i + 1], checked1 : false})

                    i = i + 2
                }
            }
        }
    }

    /** @brief Добавление элемента*/
    function addingElement(cat, colorCat)
    {
        listModelCreate.append({cat: cat, colorCat: colorCat, checked1 : false})
    }

    /** @brief Удаление элемента*/
    function removeElement(cat)
    {
        for (let i = 0; i < listModelCreate.count;i++)
        {
            if (listModelCreate.get(i).cat === cat)
            {
                listModelCreate.remove(i)
                break
            }
        }
    }

    /** @brief Получение строкого значения массива выбранных категорий*/
    function selectCheckedCats() {
        var list = ""
        for (let i = 0; i < listModelCreate.count;i++)
        {
            if (listModelCreate.get(i).checked1)
            {
                list += listModelCreate.get(i).cat + ","
            }
        }
        if (list.length > 0)
            list = list.substr(0, list.length - 1)
        console.log(list)
        return list

    }

    /** @brief Переключение всех чекбоксов категорий на режим "не нажат"*/
    function clearChecked() {

        for (let k = 0; k < listModelCreate.count;k++)
        {
            listModelCreate.get(k).checked1 = false
        }

    }

    /** @brief Выборочное активация чекбоксов*/
    function makeChecked(categories) {

        var has
        for (let k = 0; k < listModelCreate.count;k++)
        {
            has = false
            for (var j in categories)
            {
                if (categories[j] === listModelCreate.get(k).cat)
                {
                    listModelCreate.get(k).checked1 = true
                    has = true
                    break
                }
            }
            if (!has)
                listModelCreate.get(k).checked1 = false
        }


    }

    /** @brief Поиск с возврат категории с именем cat*/
    function findCategory(cat) {
        for (let i = 0; i < listModelCreate.count; i++)
        {
            if (db.toLower(cat) === db.toLower(listModelCreate.get(i).cat))
                return listModelCreate.get(i).colorCat
        }
        return ""
    }


    Component {
        id: component2Category

        Rectangle {
            width: catgrid.cellWidth - 7
            height: catgrid.cellHeight - 3
            color: colorCat
            radius: 5
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    check.checked = !check.checked
                }
            }

            TextArea{
                id: catText
                anchors.top: parent.top
                anchors.left: parent.left
                width: parent.width - 30
                height: parent.height
                text: cat.substr(0,8) + ((cat.length > 8) ? "..." : "")
                // anchors.horizontalCenter: parent.horizontalCenter
                clip: false
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                enabled: false

                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 9
                color: "white"

                // ScrollBar.vertical: null
            }

            CheckBox {
                id: check
                //checked: false
                scale: 0.5
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                //anchors.rightMargin: 5
                anchors.top: parent.top
                anchors.topMargin: 3
                checked: (checked1) ? true : false
                indicator: Rectangle {
                    implicitWidth: 26
                    implicitHeight: 26
                    color:"white"
                    opacity: 0.7
                    Rectangle {
                        width: 14
                        height: 14
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        color: "black"
                        opacity: 0.8
                        visible: check.checked
                    }
                }
                onCheckedChanged: {
                    if (checked)
                    {
                        listModelCreate.get(index).checked1 = true

                        //guide_nw.anchors.topMargin = selectSearchCategory.height + 10
                        //guide_nw.prompt_text = qsTr("Enter note") + settings.emptyString
                    }
                    else
                    {
                        listModelCreate.get(index).checked1 = false
                    }
                }

            }

        }

    }

}
