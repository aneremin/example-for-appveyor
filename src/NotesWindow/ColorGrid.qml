/**
  * @file
  * @brief Грид цветов
  *
  * Грид выбора цвета
*/

import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0


/*!
    \class ColorGrid
    \brief Грид для выбора цвета*
*/
GridView {
    id: colorgrid

    clip: true
    cellWidth: cellHeight * 1.7
    cellHeight: 50

    delegate: componentCategory
    model: listModel
    interactive: true
    /* модель с данными о каждом цвете */
    ListModel
    {
        id: listModel
        ListElement{colorCat: "#e32d43"}
        ListElement{colorCat: "#54262b"}
        ListElement{colorCat: "#50bf3f"}
        ListElement{colorCat: "#3f63bf"}
        ListElement{colorCat: "#ccdb27"}

    }

    /** @brief Получение текущего цвета в гриде*/
    function getCurrentColor() {
        return listModel.get(colorGrid.currentIndex).colorCat
    }

    /* Компонент для отображения элементов */
    Component {
        id: componentCategory

        Rectangle {
            width: colorgrid.cellWidth - 10
            height: colorgrid.cellHeight - 6
            color: colorCat
            radius: 5

            Rectangle{
                id: closeCat
                anchors.fill: parent
                //anchors.margins: 3
                anchors.rightMargin: 7
                anchors.leftMargin: 7
                scale: 0.4
                color: "transparent"
                Image {
                    anchors.fill: parent
                    //anchors.margins: 2
                    visible: (index === colorgrid.currentIndex) ? true : false
                    id: selected_img
                    source: "/resources/img/make_main.png"
                }


            }
            Rectangle {
                anchors.fill: parent
                color: "#f5ebec"
                visible: (index === colorgrid.currentIndex) ? true : false
                opacity: 0.2
                radius: 5

            }
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: colorgrid.currentIndex = index
            }
        }

    }
}


