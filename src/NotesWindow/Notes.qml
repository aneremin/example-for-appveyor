/**
  * @file
  * @brief Грид с заметками
  *
*/
import QtQuick 2.10
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0

/*!
    \class Notes
    \brief Грид с заметками
 */
Item {

    id: notesTable

    property string textColor

    ListModel {
        id: list
        dynamicRoles: true
        Component.onCompleted: {

            var lst = db.selectAllTuples(4, table)

            var i = 1
            if (lst[0] > 1)
            {
                for (; i < lst[0];) {
                    append({id: lst[i], desc: lst[i + 1], cats: db.toList(lst[i + 3])})

                    i = i + 4
                }
            }
        }
    }

    /** @brief Получение id текущего элемента*/
    function getId() {
        return list.get(lv.currentIndex).id
    }

    /** @brief Очистка грида и добавление заметок, содержащих категории categories*/
    function findByCategories(categories) {
        list.clear()

        var lst = db.selectAllTuples(4, table)

        var i = 1
        if (lst[0] > 1)
        {
            for (; i < lst[0];) {

                if (db.haveIntersection(categories, db.toList(lst[i + 3])))
                    list.append({id: lst[i], desc: lst[i + 1], cats: db.toList(lst[i + 3])})

                i = i + 4
            }
        }
    }

    /** @brief Изменение заметки*/
    function editNote(ident, desc, categories) {
        if (db.haveIntersection(searchGridCategory.getAllCategories(), categories))
        {
            list.setProperty(ident,"desc",desc)
            list.setProperty(ident,"cats",categories)
        } else {
            list.remove(ident)
        }
    }

    /** @brief Получение текущего индекса*/
    function getCurrentIndex() {
        return lv.currentIndex
    }

    /** @brief Добавление элемента в грид*/
    function addingElement(cats,id) {
        if (db.haveIntersection(searchGridCategory.getAllCategories(), cats))
            list.append({id: id, desc: descEdit.text, cats: cats})

    }

    /** @brief Удаление элемента*/
    function removeRow() {
        if (lv.currentIndex !== -1)
            list.remove(lv.currentIndex)
    }

    /** @brief Поиск элементов с категорией cat*/
    function findRemovedCategory(cat) {
        var notesList = []
        var stringList = []
        for (let i = 0; i < list.count; i++)
        {
            for (var j in list.get(i).cats)
                stringList[stringList.length] = list.get(i).cats[j]
            if (db.haveIntersection([cat], stringList))
            {
                notesList[notesList.length] = i
            }
            stringList = []

        }
        return notesList
    }

    /** @brief Изменение категорий заметок*/
    function removeCategory(notesPos,category) {

        var catList = []

        for (let i = 0; i < notesPos.length; i++)
        {
            for (var j in list.get(notesPos[i]).cats)
            {
                if (list.get(notesPos[i]).cats[j] !== category)
                {
                    catList[catList.length] = list.get(notesPos[i]).cats[j]
                }
            }

            list.setProperty(notesPos[i],"cats",catList)
            catList = []

        }
    }

    /** @brief Изменение категорий заметок*/
    function selectCheckedCats(category) {
        var catsString = ""
        for (let i = 0; i < list.count;i++)
        {
            for (var j in list.get(i).cats)
            {
                if (list.get(i).cats[j] !== category)
                {
                    catsString += list.get(i).cats[j] + ","
                }
            }
            if (catsString.length > 0)
                catsString = catsString.substr(0, catsString.length - 1)
            if (!db.editTuple({categories : catsString}, table,{id: list.get(i).id}))
            {
                return false
            }
            catsString = ""
        }
        return true

    }


    Rectangle {
        anchors.fill: parent
        opacity: 0.1
        color: "black"
        radius: 10
        z: -1
    }


    Component {
        id: componentDelegate

        Item{
            id: note
            //width: lv.cellWidth
            //height: lv.cellHeight

            Behavior on width {
                NumberAnimation {
                    easing.type: Easing.InOutQuad; duration: 700;

                    /*onStarted: {
                        if (note.state === "highlighted") {
                            note.z = 100
                        }
                    }

                    onStopped: {
                        if (note.state === "normal") {
                            note.z = 1
                        }
                    }*/
                }
            }
            Behavior on height {
                NumberAnimation {easing.type: Easing.InOutQuad; duration: 700;}
            }
            Behavior on x {
                NumberAnimation {easing.type: Easing.InOutQuad; duration: 700;}
            }
            Behavior on y {
                NumberAnimation {easing.type: Easing.InOutQuad; duration: 700;}
            }
            z: (model.index === lv.currentIndex) ? 100 : 1
            state: "normal"
            states: [
                State {
                    name: "normal"
                    PropertyChanges {
                        target: note
                        width: lv.cellWidth
                        height: lv.cellHeight
                        //z: 1
                    }
                },State {
                    name: "highlighted"
                    PropertyChanges {
                        target: note
                        x: 0
                        y: 0
                        width: lv.width
                        height: lv.height
                        //z:100
                    }
                }

            ]

            Rectangle {
                id: element

                radius: 10
                //anchors.margins: 10
                width: parent.width - 20
                height: parent.height - 20
                //color: "#efedf2"
                gradient:  Gradient {
                    id: gradientStandart
                    GradientStop {
                        position: 0
                        color: "#fdfbfb"
                    }
                    GradientStop {
                        position: 1
                        color: "#ebedee"
                    }
                }

                Label {
                    id: idText
                    anchors.fill: parent
                    //anchors.horizontalCenter: parent.horizontalCenter
                    //anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Label.AlignVCenter
                    horizontalAlignment: Label.AlignHCenter
                    text: "#" + id
                    clip: true
                    font.italic: true;
                    font.pixelSize: 45
                    opacity: 0.4
                    //z: -1
                    color: "#cdcfd4"
                }
                /*Rectangle {
                    id: indicator
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    anchors.top: parent.top
                    anchors.topMargin: 5
                    width: 10
                    height: width
                    radius: 5
                    color: "#e35b5b"
                    opacity: (index === lv.currentIndex) ? 1 : 0
                }*/

                border.color: style.getBackgroundGradientStop()
                border.width: 2//(index === lv.currentIndex) ? 4 : 0
                //smooth: true

                GridCategory {
                    id: listViewGridCategory
                    objectName: "listViewGridCategory"
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.topMargin: 5
                    //anchors.leftMargin: 5
                    width: parent.width
                    height: 40
                    categories: cats
                    // height: heightCat
                }


                Rectangle {
                    anchors.top: listViewGridCategory.bottom
                    anchors.topMargin: 3
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.leftMargin: 10
                    anchors.rightMargin: 10
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 15
                    color: "transparent"
                    Flickable {
                        id: flickable
                        anchors.fill: parent
                        anchors.margins: 5
                        width: parent.width
                        height: parent.height
                        contentHeight: descRead.height
                        contentWidth: descRead.width
                        clip: true
                        function ensureVisible(r)
                        {
                            if (contentX >= r.x)
                                contentX = r.x;
                            else if (contentX+width <= r.x+r.width)
                                contentX = r.x+r.width-width;
                            if (contentY >= r.y)
                                contentY = r.y;
                            else if (contentY+height <= r.y+r.height)
                                contentY = r.y+r.height-height;
                        }
                        TextEdit {
                            property string linkString
                            width: flickable.width
                            textFormat: TextEdit.RichText
                            //bottomPadding: 10
                            id: descRead
                            clip: true
                            wrapMode: Label.WrapAnywhere
                            text: desc
                            onCursorRectangleChanged: flickable.ensureVisible(cursorRectangle)
                            font.family: db.getFontFamily(desc)
                            color: "#4d3f3f"
                            onLinkHovered: {
                                linkString = link
                            }

                            onLinkActivated: {
                                console.log("fff")
                                Qt.openUrlExternally(link)
                            }
                            /*MouseArea {
                                anchors.fill: parent
                                acceptedButtons: Qt.NoButton

                                propagateComposedEvents: true

                            }*/
                        }
                    }
                }


                /*Label {
                    id: idText
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    anchors.bottomMargin: 2
                    anchors.rightMargin: idText.width
                    text: "#" + id
                    font.pixelSize: 10
                }*/

                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        gridMenu.visible = false
                    }
                    onClicked: {

                        if (descRead.hoveredLink) {
                            Qt.openUrlExternally(descRead.linkString)
                        }
                        if(mouse.button & Qt.RightButton) {
                            lv.currentIndex = index
                            if ( Qt.RightButton)
                            {
                                gridMenu.visible = true
                                //  gridMenu.x = parent.x + mouseX
                                //  gridMenu.y = parent.y + mouseY - menushka.height
                                var pos = getAbsolutePosition(parent);
                                gridMenu.x = pos.x + mouseX
                                gridMenu.y = pos.y + mouseY
                                //console.debug(parent.y, mouseY, pos.x,pos.y)
                            }
                            else
                                gridMenu.visible = false


                            //console.debug(mouse.x, mouse.y)
                        }
                        else
                        {
                            lv.currentIndex = index
                            gridMenu.visible = false
                        }
                    }
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    cursorShape: descRead.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                    propagateComposedEvents: true
                    onDoubleClicked: {
                        note.state = (note.state === "normal") ? "highlighted" : "normal"
                        /*note_add_button.isAdd = false
                        descEdit.font.family = db.getFontFamily(desc)
                        descEdit.text = db.changeAllQuotes(desc,true)
                        selectSearchCategory.makeChecked(cats)
                        createNote.open()*/
                    }
                }
            }
        }
    }
    /** @brief Получение глобальных координат элемента*/
    function getAbsolutePosition(node) {
        var returnPos = {};
        returnPos.x = node.x;
        returnPos.y = node.y;
        var p = node.parent;
        while(p) {
            returnPos.x += p.x;
            returnPos.y += p.y;
            p = p.parent;
            if (p === notesTable) {
                break;
            }
        }
        return returnPos;
    }

    Menu {
        id: gridMenu
        font.pointSize: 10

        property string textColor: style.getTextColor()
        property string backColor: style.getBackgroundGradientStart()

        background: Rectangle {
            // anchors.fill: parent
            gradient: style.getBackgroundGradient()
            border.width: 1
            border.color: style.getTextColor()

            implicitWidth: 150
            //implicitHeight: 20
            radius: 2
        }



        MenuItem {
            contentItem: Text {
                text: qsTr("New") + settings.emptyString
                color: parent.highlighted ? gridMenu.backColor : gridMenu.textColor
                anchors.fill: parent
                font.pointSize: 10
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
            background: Rectangle {
                //gradient: parent.highlighted ? none : style.getBackgroundGradient()
                color: parent.highlighted ? gridMenu.textColor : "transparent"
                border.width: 0
                border.color: gridMenu.backColor

                implicitWidth: 130
                implicitHeight: 40
                radius: 2
            }

            onClicked: {
                note_add_button.isAdd = true
                createNote.open()
            }
        }
        MenuItem {
            Text {
                text: qsTr("Edit") + settings.emptyString
                color: parent.highlighted ? gridMenu.backColor : gridMenu.textColor
                anchors.fill: parent
                font.pointSize: 10
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
            background: Rectangle {
                //gradient: parent.highlighted ? none : style.getBackgroundGradient()
                color: parent.highlighted ? gridMenu.textColor : "transparent"
                border.width: 0
                border.color: gridMenu.backColor

                implicitWidth: 130
                implicitHeight: 40
                radius: 2
            }

            onClicked: {

                note_add_button.isAdd = false
                descEdit.font.family = db.getFontFamily(list.get(lv.currentIndex).desc)
                descEdit.text = db.changeAllQuotes(list.get(lv.currentIndex).desc,true)
                selectSearchCategory.makeChecked(list.get(lv.currentIndex).cats)
                createNote.open()
            }
        }

        MenuItem {
            Text {
                text: qsTr("Remove") + settings.emptyString
                color: parent.highlighted ? gridMenu.backColor : gridMenu.textColor
                anchors.fill: parent
                font.pointSize: 10
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
            background: Rectangle {
                //gradient: parent.highlighted ? none : style.getBackgroundGradient()
                color: parent.highlighted ? gridMenu.textColor : "transparent"
                border.width: 0
                border.color: gridMenu.backColor

                implicitWidth: 130
                implicitHeight: 40
                radius: 2
            }

            onClicked: {

                if (db.removeTuple({ id : notes.getId()}, table))
                {
                    // var id_ = notes.getId()
                    // console.log(db.toInt(id_))
                    notes.removeRow()
                }
                else
                {
                    error.log = qsTr("Error deleting database row") + settings.emptyString
                    error.err_name = qsTr("Error deleting database row: ") + settings.emptyString + db.getError();
                    error.open()
                }
            }
        }
    }




    GridView {
        id: lv

        /*MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            onPressed: {
                inputRect.state = "hide"
            }
        }*/

        cellWidth: 250
        cellHeight: cellWidth * 0.8
        anchors.fill: parent
        anchors.margins: 10
        model: list
        clip: true
        delegate: componentDelegate

        onCountChanged: {
            if (count > 0)
                emptyText.visible = false
            else
                emptyText.visible = true
        }

        Text {
            id: emptyText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("Here is empty") + settings.emptyString
            color: textColor
            font.pixelSize: 15
            visible: false
            opacity: 0.6
        }

        header: Rectangle {
            height: 20
        }

        highlight: Rectangle {
            /*width: 1
            height: 1
            border.width: 2
            border.color: "#CE1A33"
            radius: 4*/
            color: "transparent"
            z: 101
            x: (lv.currentItem) ? lv.currentItem.x : 0
            y: (lv.currentItem) ? lv.currentItem.y : 0
            Behavior on x { SpringAnimation { spring: 3; damping: 0.2 } }
            Behavior on y { SpringAnimation { spring: 3; damping: 0.2 } }
            Text {
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.horizontalCenterOffset: -5

                text: qsTr("current") + settings.emptyString
                color: "#C12522"
            }
        }

        add: Transition {
            NumberAnimation { properties: "x,y"; easing.amplitude: 40;easing.type: Easing.InOutQuad; duration: 500 }
            //SpringAnimation { spring: 3; damping: 0.2 }
        }
        displaced: Transition {
            NumberAnimation { properties: "x,y"; easing.type: Easing.InOutQuad;duration: 500 }
        }

        remove: Transition {
            ParallelAnimation {
                NumberAnimation { property: "opacity"; to: 0; duration: 700 }
                NumberAnimation { properties: "x,y"; to: 100; duration: 1000 }
            }
        }

    }

}
