/**
  * @file
  * @brief Страница для работы с заметками
  *
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.3
import "../Other" as OtherModules
import "../Style" as StyleModules

/*!
    \class NotesWindow
    \brief Страница для работы с заметками
 */
Page {
    id: notesWindow

    /** @brief Сигнал измненения категорий*/
    signal countOfCategoriesChanged
    /** @brief Название таблицы заметок в БД*/
    property string table: "notesList"
    /** @brief Название таблицы категорий в БД*/
    property string catsTable: "catsList"

    Action {
        shortcut: "Esc"
        onTriggered: {
            inputRect.state = "hide"
        }
    }

    background: LinearGradient {
        id: lg
        anchors.fill: parent
        start: Qt.point(0,0)
        end: Qt.point(parent.width, parent.height)
        gradient: style.getBackgroundGradient()

    }

    z: 5
    onCountOfCategoriesChanged: {

        notes.findByCategories(searchGridCategory.getAllCategories())
    }

    Column {
        id: scrcol
        width: parent.width
        spacing: 30
        //anchors.fill: parent
        anchors.top: parent.top
        anchors.topMargin: 20
        //anchors.horizontalCenter: parent.horizontalCenter

        // строка поиска посередине
        TextField {
            id: search
            objectName: "search"
            //  z:1

            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * 2 / 3

            placeholderText: qsTr("Enter category (or categories througth ';')") + settings.emptyString
            placeholderTextColor: style.getPlaceholderTextColor()
            color: style.getTextColor()
            background: Rectangle {
                implicitWidth: parent.width
                implicitHeight: parent.height
                color: "transparent"
                border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                border.width: 3
                radius: 5
            }

            onEditingFinished: {
                searchGridCategory.findByName(db.toList(text,";"))
                rightGrid.editSearchCategory = false
                rightGrid.makeChecked(searchGridCategory.getAllCategories())
                rightGrid.editSearchCategory = true
                clear()
            }
        }
        OtherModules.StylishButton {
            id: createCategoryBtn
            button_name: qsTr("+ Create") + settings.emptyString
            background_gradient: style.getGradientStandart()
            entered_gradient: style.getGradientEntered()
            anchors.verticalCenter: search.verticalCenter
            anchors.left: search.right
            anchors.leftMargin: 10
            //image_src: "/resources/img/add.png"
            width: parent.width / 10//105
            height: 25
            onMouseAreaClicked: {
                addCategory.open()
            }
        }

        Row {
            id: rowCat
            spacing: 5
            anchors.top: search.bottom
            anchors.topMargin: 7
            anchors.left: search.left
            width: search.width
            SearchCategory {
                id: searchGridCategory

                width: search.width - 110
                height: 36
            }

            OtherModules.StylishButton {

                id: catButton
                button_name: qsTr("> More") + settings.emptyString
                background_gradient: style.getGrayGradientStandart()
                entered_gradient: style.getGrayGradientEntered()
                anchors.verticalCenter: rowCat.verticalCenter
                //image_src: "/resources/img/add.png"
                width: 105
                height: 25
                opacity: 0.7
                onMouseAreaClicked: {
                    inputRect.state = "show"
                }
            }

        }

        Notes {
            id: notes
            anchors.top: rowCat.bottom
            anchors.topMargin: 7
            width: parent.width / 9 * 8
            height: notesWindow.height * 5 / 7
            clip: false
            anchors.horizontalCenter: parent.horizontalCenter
            textColor: style.getTextColor()
            // кнопки слева сверху
            Column {
                id: buttonsNoteRow
                objectName: "buttonsNoteRow"
                anchors.right: parent.left
                anchors.top: parent.top
                anchors.rightMargin: 5

                width: notes.width / 23
                spacing: 10
                Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }

                //property bool isEntered: false
                OtherModules.StylishButton {
                    id: new_button

                    image_src: "/resources/img/add.svg"

                    objectName: "newNoteButton"


                    // width: 200
                    width: parent.width
                    //height: 60
                    //button_name: qsTr("New") + settings.emptyString
                    background_gradient: style.getGradientStandart()
                    entered_gradient: style.getGradientEntered()
                    //border_color: style.getBorderColor()
                    //text_color: style.getTextColor()

                    onMouseAreaClicked: {

                        note_add_button.isAdd = true
                        createNote.open()
                    }
                }

                OtherModules.StylishButton {
                    id: delete_button

                    image_src: "/resources/img/remove.svg"

                    //   width: 200
                    width: parent.width

                    //button_name: qsTr("Remove") + settings.emptyString
                    background_gradient: style.getGradientStandart()
                    entered_gradient: style.getGradientEntered()


                    onMouseAreaClicked: {

                        if (db.removeTuple({ id : notes.getId()}, table))
                        {
                            // var id_ = notes.getId()
                            // console.log(db.toInt(id_))
                            notes.removeRow()
                        }
                        else
                        {
                            error.log = qsTr("Error deleting database row") + settings.emptyString
                            error.err_name = qsTr("Error deleting database row: ") + settings.emptyString + db.getError();
                            error.open()
                        }
                    }
                }
            }

        }

        Popup {

            id: addCategory

            z:5
            x: parent.width / 2 - width / 2
            y: notes.y
            width: notesWindow.width * 0.36
            height: notesWindow.height * 0.62
            modal: true
            focus: true

            background:  Rectangle {
                gradient: style.getBackgroundGradient()
                radius: 10
                z: -1
            }

            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

            contentItem: Item {
                id: addCategoryItem
                anchors.fill: parent
                anchors.margins: 10
                Column {
                    id: addCategoryColumn
                    width: addCategory.width
                    spacing: 15
                    Row {

                        height: 30
                        width: parent.width - 10
                        spacing: 10
                        Label {
                            id: categoryLabel
                            text: qsTr("Category: ") + settings.emptyString
                            color: style.getTextColor()
                            height: 30
                            verticalAlignment: Label.AlignVCenter
                        }

                        TextField {
                            id: cateEdit

                            width: parent.width - categoryLabel.width - 20
                            height: 30
                            anchors.verticalCenter: categoryLabel.verticalCenter
                            selectByMouse: true
                            placeholderText: qsTr("Enter name") + settings.emptyString
                            placeholderTextColor: style.getPlaceholderTextColor()
                            color: style.getTextColor()
                            //text: rezultText

                            // языкозависимый параметр
                            validator: RegExpValidator {regExp: /[А-Яа-яA-Za-z_]{0,}/}

                            background: Rectangle {
                                implicitWidth: parent.width
                                implicitHeight: parent.height
                                color: "transparent"
                                border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                border.width: 3
                                radius: 5
                            }
                        }

                    }
                    Row {

                        height: 30
                        width: parent.width
                        spacing: 10
                        Label {
                            id: colorLabel
                            text: qsTr("Color: ") + settings.emptyString
                            color: style.getTextColor()
                            height: 30
                            width: categoryLabel.width
                            verticalAlignment: Label.AlignVCenter
                        }
                        ColorGrid {
                            id: colorGrid
                            height: 100
                            width: parent.width - colorLabel.width - 5
                            interactive: true
                        }
                    }
                }

                Row {
                    id: rowInputBtns
                    objectName: "rowInputCenterBtns"
                    //anchors.top: rowBtns.top
                    anchors.right: parent.right
                    anchors.rightMargin: 7

                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 7
                    //width: charactersTable.width / 14
                    height: 30

                    spacing: 5
                    OtherModules.StylishButton {
                        id: add_button
                        objectName: "createButton"

                        //image_src: "/resources/img/add.png"

                        width: 80
                        Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                        button_name: qsTr("Create") + settings.emptyString
                        background_gradient: style.getGradientStandart()
                        entered_gradient: style.getGradientEntered()

                        onMouseAreaClicked: {

                            if ((cateEdit.text === ""))
                            {
                                visibleErrorDialog = true
                                log = qsTr("Enter category!") + settings.emptyString
                                errName = qsTr("Enter category!") + settings.emptyString
                                errorLoader.sourceComponent = errorComponent

                            }
                            else if (db.selectWithCond({category : cateEdit.text},2,catsTable).length > 1)
                            {
                                visibleErrorDialog = true
                                log = qsTr("This category already exists!") + settings.emptyString
                                errName = qsTr("This category already exists!") + settings.emptyString
                                errorLoader.sourceComponent = errorComponent

                            }
                            else
                            {
                                if (db.addTuple([cateEdit.text,colorGrid.getCurrentColor()], catsTable))
                                {
                                    rightGrid.addingElementRight()
                                    selectSearchCategory.addingElement(cateEdit.text,colorGrid.getCurrentColor())
                                    cateEdit.clear()
                                    addCategory.close()
                                }
                                else
                                {
                                    //console.debug("Ошибка добавления строки в БД")
                                    visibleErrorDialog = true
                                    log = qsTr("Error adding database row") + settings.emptyString
                                    errName = qsTr("Error adding database row: ") + settings.emptyString + db.getError()
                                    errorLoader.sourceComponent = errorComponent
                                    //error.log = "Ошибка добавления строки в базу данных";
                                    //error.err_name = "Ошибка добавления строки в базу данных: " + db.getError();
                                    //error.open();

                                }
                            }

                        }
                    }
                    OtherModules.StylishButton {
                        id: cancel_button
                        objectName: "cancelButton"
                        width: 80
                        Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                        button_name: qsTr("Cancel") + settings.emptyString
                        background_gradient: style.getGrayGradientStandart()
                        entered_gradient: style.getGrayGradientEntered()

                        onMouseAreaClicked: {
                            cateEdit.clear()
                            addCategory.close()
                        }
                    }

                }
            }
        }


        Popup {

            id: createNote


            x: parent.width / 2 - width / 2
            y: search.y
            width: notesWindow.width * 0.6
            height: notesWindow.height * 0.8
            modal: true
            focus: true
            background:  Rectangle {
                gradient: style.getBackgroundGradient()
                radius: 10
                z: -1
            }


            onOpened: {
                if (note_add_button.isAdd) {
                    descEdit.clear()
                    selectSearchCategory.clearChecked()
                } else {
                    fontBox.setIndex()
                }
            }

            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

            contentItem: Item {
                id: createNoteItem
                anchors.fill: parent
                anchors.margins: 10
                Column {
                    id: createNoteSelectCategory
                    width: createNote.width
                    spacing: 10
                    Row {

                        height: 80
                        width: parent.width - 10
                        spacing: 10
                        Label {
                            id: catNoteLabel
                            text: qsTr("Category: ") + settings.emptyString
                            color: style.getTextColor()
                            height: 30
                            verticalAlignment: Label.AlignVCenter
                        }


                        CreateCategory {
                            id: selectSearchCategory
                            width: parent.width - catNoteLabel.width - 5
                            height: 79
                        }

                    }
                    Row {

                        height: 25
                        width: parent.width - 10
                        spacing: 4
                        ComboBox {
                            id: fontBox

                            height: parent.height
                            width: parent.width/3
                            leftPadding: 5
                            model: ["Courier", "Helvetica"]
                            currentIndex: 1
                            function setIndex() {
                                var fontFamily = db.getFontFamily(descEdit.text)

                                for (let i = 0; i < model.length; i++) {

                                    if (model[i] === fontFamily)
                                    {
                                        currentIndex = i
                                        console.log(">>>",i)
                                        console.log(">>>",fontFamily)
                                        console.log(">>>",descEdit.text)
                                        console.log(">>>",fontBox.valueAt(i))
                                        return;
                                    }
                                }
                                currentIndex = 1
                            }

                            onCurrentIndexChanged: {

                                descEdit.font.family = fontBox.valueAt(fontBox.currentIndex)
                                descEdit.text = db.changeFontFamily(descEdit.text,fontBox.valueAt(fontBox.currentIndex))
                            }

                            // индикация при выборе
                            delegate: ItemDelegate {
                                width: fontBox.width

                                Rectangle {
                                    anchors.fill: parent
                                    color: style.getSdIndicatorColorChecked()
                                    opacity: 0.9
                                }

                                contentItem: Text {
                                    text: modelData
                                    color: "white"
                                    font: fontBox.font
                                    elide: Text.ElideRight
                                    verticalAlignment: Text.AlignVCenter
                                }

                                highlighted: fontBox.highlightedIndex === index
                            }


                            // вид выбранного значения
                            contentItem: Text {
                                leftPadding: 0
                                rightPadding: fontBox.indicator.width + fontBox.spacing

                                text: fontBox.displayText
                                font: fontBox.font
                                color: fontBox.pressed ? style.getSdTextColorDown() : style.getSdTextColor()
                                verticalAlignment: Text.AlignVCenter
                                elide: Text.ElideRight
                            }

                            background: Rectangle {
                                implicitWidth: 120
                                implicitHeight: 30
                                color: "transparent"
                                border.color: fontBox.pressed ? style.getSdIndicatorBorderColor() : style.getSdBackgroundBorderColor()
                                border.width: fontBox.pressed ? 3 : 2
                                radius: 5
                            }

                            indicator: Rectangle {
                                width: 5
                                height: width
                                radius: 3
                                color: "#ffffff"
                            }

                            // вид списка со значениями
                            popup: Popup {
                                y: fontBox.height - 1
                                width: fontBox.width
                                implicitHeight: contentItem.implicitHeight
                                padding: 1

                                contentItem: ListView {

                                    clip: true

                                    //implicitHeight: contentHeight
                                    implicitHeight: 80
                                    model: fontBox.popup.visible ? fontBox.delegateModel : null
                                    currentIndex: fontBox.currentIndex
                                    ScrollIndicator.vertical: ScrollIndicator { }
                                }

                                background: Rectangle {
                                    color: style.getSdIndicatorColorChecked()
                                    opacity: 0.7
                                    border.color: style.getSdIndicatorBorderColor()
                                    radius: 2
                                }
                            }
                        }

                        OtherModules.StylishButton {
                            id: addReferenceBtn
                            objectName: "addReferenceBtn"
                            //anchors.top: catNoteLabel.bottom
                            image_src: "/resources/img/reference.png"

                            width: parent.height
                            height: width
                            Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                            //button_name: qsTr("ref") + settings.emptyString
                            background_gradient: style.getGradientStandart()
                            entered_gradient: style.getGradientEntered()

                            onMouseAreaClicked: {
                                createReference.open()
                                // descEdit.insert(descEdit.cursorPosition,"<a href = 'http://google.com'>google</a>")
                                //descEdit.append()
                            }
                        }
                        OtherModules.StylishButton {
                            id: addImageBtn
                            objectName: "addReferenceBtn"
                            //anchors.top: catNoteLabel.bottom
                            image_src: "/resources/img/human.png"

                            width: parent.height
                            height: width
                            Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                            //button_name: qsTr("ref") + settings.emptyString
                            background_gradient: style.getGradientStandart()
                            entered_gradient: style.getGradientEntered()

                            onMouseAreaClicked: {
                                fileDialog.open()
                                // descEdit.insert(descEdit.cursorPosition,"<a href = 'http://google.com'>google</a>")
                                //descEdit.append()
                            }

                            FileDialog {
                                id: fileDialog
                                visible: false
                                title: selectFileText
                                nameFilters: [ "Image files (*.jpg *.png *.svg *.gif)", "All files (*)" ]
                                folder: shortcuts.home
                                onAccepted: {
                                    console.log("You chose: " + fileDialog.fileUrl)
                                    descEdit.insert(descEdit.cursorPosition,"<img width = '"+ "100" +"' src = '" + fileDialog.fileUrl + "'>")

                                }
                                onRejected: {
                                    console.log("Canceled")
                                }
                            }
                        }
                        Popup {

                            id: createReference


                            x: -10
                            //y: notes.y
                            width: notesWindow.width * 0.36
                            height: notesWindow.height * 0.2
                            modal: true
                            focus: true
                            background:  Rectangle {
                                gradient: style.getBackgroundGradient()
                                radius: 10
                                z: -1
                            }


                            /*onOpened: {
                                if (note_add_button.isAdd) {
                                    descEdit.clear()
                                    selectSearchCategory.clearChecked()
                                }
                            }*/
                            onClosed: {
                                referenceEdit.clear()
                                referenceNameEdit.clear()
                            }

                            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

                            contentItem: Item {
                                id: createReferenceItem
                                anchors.fill: parent
                                anchors.margins: 10
                                Column {
                                    id: createReferenceColumn
                                    width: addCategory.width
                                    spacing: 10
                                    TextField {
                                        id: referenceEdit

                                        width: parent.width / 2
                                        height: 30
                                        //height: 40

                                        placeholderText: qsTr("http://example.com") + settings.emptyString
                                        placeholderTextColor:"#fffddd"// style.getPlaceholderTextColor()
                                        color: "white"//style.getTextColor()
                                        background: Rectangle {
                                            implicitWidth: parent.width
                                            implicitHeight: parent.height
                                            color: "transparent"
                                            border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                            border.width: 3
                                            radius: 5


                                        }
                                    }

                                    TextField {
                                        id: referenceNameEdit

                                        width: createReferenceColumn.width / 2
                                        height: 30
                                        //height: 40

                                        placeholderText: qsTr("example") + settings.emptyString
                                        placeholderTextColor:"#fffddd"// style.getPlaceholderTextColor()
                                        color: "white"//style.getTextColor()
                                        background: Rectangle {
                                            implicitWidth: parent.width
                                            implicitHeight: parent.height
                                            color: "transparent"
                                            border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                            border.width: 3
                                            radius: 5


                                        }
                                    }

                                }
                                OtherModules.StylishButton {
                                    id: createReferenceBtn
                                    objectName: "createReferenceBtn"
                                    anchors {
                                        bottom: parent.bottom
                                        right: parent.right
                                        bottomMargin: 10
                                        rightMargin: 10
                                    }

                                    //anchors.top: catNoteLabel.bottom
                                    //image_src: "/resources/img/reference.png"
                                    button_name: qsTr("Add") + settings.emptyString
                                    width: 50
                                    height: 30
                                    Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                                    //button_name: qsTr("ref") + settings.emptyString
                                    background_gradient: style.getGradientStandart()
                                    entered_gradient: style.getGradientEntered()

                                    onMouseAreaClicked: {
                                        if (referenceEdit.text.length && referenceNameEdit.text.length) {
                                            descEdit.insert(descEdit.cursorPosition,"<a href = '" + referenceEdit.text + "'>"
                                                            + referenceNameEdit.text + "</a>")
                                        }
                                        createReference.close()
                                        //descEdit.append()
                                    }
                                }
                            }
                        }

                        OtherModules.StylishButton {
                            id: addBoldBtn
                            objectName: "addBoldBtn"
                            //anchors.top: catNoteLabel.bottom
                            //image_src: "/resources/img/reference.png"
                            button_name: "B"
                            width: parent.height
                            height: width
                            Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                            //button_name: qsTr("ref") + settings.emptyString
                            background_gradient: style.getGradientStandart()
                            entered_gradient: style.getGradientEntered()

                            onMouseAreaClicked: {
                                let formattedText = descEdit.getFormattedText(descEdit.selectionStart,descEdit.selectionEnd)
                                let selectedText = descEdit.selectedText
                                descEdit.remove(descEdit.selectionStart,descEdit.selectionEnd)
                                descEdit.insert(descEdit.cursorPosition,db.changeBold(formattedText,selectedText))
                                /*console.log(descEdit.getFormattedText(descEdit.selectionStart,descEdit.selectionEnd))
                                let selectedText = descEdit.selectedText
                                descEdit.remove(descEdit.selectionStart,descEdit.selectionEnd)
                                descEdit.insert(descEdit.cursorPosition,"<b>" + selectedText + "</b>")*/

                                //descEdit.append()
                            }
                        }
                        OtherModules.StylishButton {
                            id: addItalicBtn
                            objectName: "addItalicBtn"
                            //anchors.top: catNoteLabel.bottom
                            //image_src: "/resources/img/reference.png"
                            button_name: "I"
                            width: parent.height
                            height: width
                            Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                            //button_name: qsTr("ref") + settings.emptyString
                            background_gradient: style.getGradientStandart()
                            entered_gradient: style.getGradientEntered()

                            onMouseAreaClicked: {
                                let formattedText = descEdit.getFormattedText(descEdit.selectionStart,descEdit.selectionEnd)
                                let selectedText = descEdit.selectedText
                                descEdit.remove(descEdit.selectionStart,descEdit.selectionEnd)
                                descEdit.insert(descEdit.cursorPosition,db.changeItalic(formattedText,selectedText))

                                console.log(descEdit.text)
                                //descEdit.insert(descEdit.cursorPosition,db.changeBold(formattedText,selectedText))
                                /*console.log(descEdit.getFormattedText(descEdit.selectionStart,descEdit.selectionEnd))
                                let selectedText = descEdit.selectedText
                                descEdit.remove(descEdit.selectionStart,descEdit.selectionEnd)
                                descEdit.insert(descEdit.cursorPosition,"<b>" + selectedText + "</b>")*/

                                //descEdit.append()
                            }
                        }
                        OtherModules.StylishButton {
                            id: addUnderlineBtn
                            objectName: "addUnderlineBtn"
                            //anchors.top: catNoteLabel.bottom
                            //image_src: "/resources/img/reference.png"
                            button_name: "U"
                            width: parent.height
                            height: width
                            Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                            //button_name: qsTr("ref") + settings.emptyString
                            background_gradient: style.getGradientStandart()
                            entered_gradient: style.getGradientEntered()

                            onMouseAreaClicked: {
                                let formattedText = descEdit.getFormattedText(descEdit.selectionStart,descEdit.selectionEnd)
                                let selectedText = descEdit.selectedText
                                descEdit.remove(descEdit.selectionStart,descEdit.selectionEnd)
                                descEdit.insert(descEdit.cursorPosition,db.changeUnderline(formattedText,selectedText))
                                //descEdit.insert(descEdit.cursorPosition,"<u>" + selectedText + "</u>")
                                console.log("!",descEdit.text)
                                //descEdit.insert(descEdit.cursorPosition,db.changeBold(formattedText,selectedText))
                                /*console.log(descEdit.getFormattedText(descEdit.selectionStart,descEdit.selectionEnd))
                                let selectedText = descEdit.selectedText
                                descEdit.remove(descEdit.selectionStart,descEdit.selectionEnd)
                                descEdit.insert(descEdit.cursorPosition,"<b>" + selectedText + "</b>")*/

                                //descEdit.append()
                            }
                        }
                    }
                    Rectangle {
                        id: descRow
                        width: parent.width - 20
                        /*anchors {
                            top: selectSearchCategory.bottom
                            //topMargin: 20
                            right: createNoteItem.right
                            //rightMargin: 20
                            left: createNoteItem.left
                            bottom: note_add_button.top
                            margins: 20

                        }*/
                        height: createNoteItem.height - 170
                        border.color: descEdit.activeFocus ? "#B175C5" : "#6C669D"
                        border.width: 3
                        radius: 5
                        //height: 120
                        //width: parent.width - 20
                        color: "transparent"
                        Flickable {
                            id: flick
                            anchors.fill: parent
                            anchors.margins: 5
                            width: parent.width
                            height: parent.height
                            contentHeight: descEdit.height
                            contentWidth: descEdit.width
                            clip: true
                            function ensureVisible(r)
                            {
                                if (contentX >= r.x)
                                    contentX = r.x;
                                else if (contentX+width <= r.x+r.width)
                                    contentX = r.x+r.width-width;
                                if (contentY >= r.y)
                                    contentY = r.y;
                                else if (contentY+height <= r.y+r.height)
                                    contentY = r.y+r.height-height;
                            }
                            TextEdit {
                                id: descEdit
                                //anchors.fill: parent
                                width: flick.width
                                objectName: "descEdit"
                                //hoveredLink: "qq"
                                //anchors.horizontalCenter: parent.horizontalCenter
                                clip: true
                                onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                                //text: "<b>Hello</b> <i>World!</i>"
                                font.family: "Helvetica"
                                font.pointSize: 10
                                selectByMouse: true
                                wrapMode: TextEdit.WrapAnywhere
                                textFormat: TextEdit.RichText
                                color: style.getTextColor()
                                focus: true
                                Keys.onSpacePressed: {

                                    descEdit.insert(descEdit.cursorPosition,"<p>&nbsp;</p>")
                                }

                            }
                            /*TextArea {
                                id: descEdit
                                objectName: "descEdit"
                                //hoveredLink: "qq"
                                //anchors.horizontalCenter: parent.horizontalCenter
                                width: parent.width
                                height: parent.height
                                clip: true
                                wrapMode: TextArea.WrapAnywhere
                                selectByMouse: true
                                placeholderText: qsTr("Write...") + settings.emptyString
                                placeholderTextColor: "#fffddd"//style.getPlaceholderTextColor()
                                color: style.getTextColor()
                                background: Rectangle {
                                    width: descRow.width
                                    height: descRow.height
                                    clip: true
                                    color: "transparent"
                                    border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                    border.width: 3
                                    radius: 5
                                }

                                KeyNavigation.tab: note_add_button


                            }*/
                        }
                    }
                }

                Row {
                    id: rowInpuNotetBtns
                    objectName: "rowInputCenterBtns"
                    //anchors.top: rowBtns.top
                    anchors.right: parent.right
                    anchors.rightMargin: 7

                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 7
                    //width: charactersTable.width / 14
                    height: 30

                    spacing: 5
                    OtherModules.StylishButton {
                        id: note_add_button
                        objectName: "createButton"
                        property bool isAdd: true
                        //image_src: "/resources/img/add.png"

                        width: 80
                        Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                        button_name: (isAdd) ? qsTr("Create") + settings.emptyString :
                                               qsTr("Edit") + settings.emptyString
                        background_gradient: style.getGradientStandart()
                        entered_gradient: style.getGradientEntered()

                        onMouseAreaClicked: {

                            if ((descEdit.text === ""))
                            {
                                visibleErrorDialog = true
                                log = qsTr("Enter description!") + settings.emptyString
                                errName = qsTr("Enter description!") + settings.emptyString
                                errorLoader.sourceComponent = errorComponent

                            }
                            else
                            {
                                var cts = selectSearchCategory.selectCheckedCats()

                                if (isAdd) {
                                    if (db.addTuple([db.changeAllQuotes(descEdit.text,false),
                                                     cts], table))
                                    {
                                        notes.addingElement(db.toList(cts),db.getId())
                                        descEdit.clear()
                                        selectSearchCategory.clearChecked()
                                        createNote.close()
                                        //countOfCategoriesChanged()
                                    }
                                    else
                                    {
                                        //console.debug("Ошибка добавления строки в БД")
                                        visibleErrorDialog = true
                                        log = qsTr("Error adding database row") + settings.emptyString
                                        errName = qsTr("Error adding database row: ") + settings.emptyString + db.getError()
                                        errorLoader.sourceComponent = errorComponent
                                        //error.log = "Ошибка добавления строки в базу данных";
                                        //error.err_name = "Ошибка добавления строки в базу данных: " + db.getError();
                                        //error.open();

                                    }
                                }
                                else
                                {
                                    if (db.editTuple({note: db.changeAllQuotes(descEdit.text,false),
                                                         categories : cts}, table,{id: notes.getId()}))

                                    {
                                        notes.editNote(notes.getCurrentIndex(),descEdit.text,db.toList(cts))
                                        descEdit.clear()
                                        selectSearchCategory.clearChecked()
                                        createNote.close()
                                        //countOfCategoriesChanged()

                                    }
                                    else
                                    {
                                        //console.debug("Ошибка добавления строки в БД")
                                        visibleErrorDialog = true
                                        log = qsTr("Error editing database row") + settings.emptyString
                                        errName = qsTr("Error editing database row: ") + settings.emptyString + db.getError()
                                        errorLoader.sourceComponent = errorComponent
                                        //error.log = "Ошибка добавления строки в базу данных";
                                        //error.err_name = "Ошибка добавления строки в базу данных: " + db.getError();
                                        //error.open();

                                    }
                                }
                            }

                        }
                    }
                    OtherModules.StylishButton {
                        id: note_cancel_button
                        objectName: "cancelButton"
                        width: 80
                        Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                        button_name: qsTr("Cancel") + settings.emptyString
                        background_gradient: style.getGrayGradientStandart()
                        entered_gradient: style.getGrayGradientEntered()

                        onMouseAreaClicked: {
                            descEdit.clear()
                            selectSearchCategory.clearChecked()
                            createNote.close()
                        }
                    }

                }
            }
        }

        Popup {

            id: haveDependentElement

            property int index: -1
            property string category: ""
            property var notesWithCategory
            x: parent.width / 2 - width / 2
            y: notes.y
            width: notesWindow.width * 0.36
            height: notesWindow.height * 0.62
            modal: true
            focus: true
            background:  Rectangle {
                gradient: style.getBackgroundGradient()
                radius: 10
                z: -1
            }
            //closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

            contentItem: Item {
                anchors.fill: parent
                anchors.margins: 10

                Label {
                    color: style.getTextColor()
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.topMargin: 10
                    anchors.leftMargin: 10
                    text: qsTr("Category have dependent elements. Delete?") + settings.emptyString
                }

                Row {

                    objectName: "rowInputCenterBtns"
                    //anchors.top: rowBtns.top
                    anchors.right: parent.right
                    anchors.rightMargin: 7

                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 7
                    //width: charactersTable.width / 14
                    height: 30

                    spacing: 5
                    OtherModules.StylishButton {
                        id: ok
                        objectName: "okButton"

                        //image_src: "/resources/img/add.png"

                        width: 80
                        Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                        button_name: qsTr("Yes") + settings.emptyString
                        background_gradient: style.getGradientStandart()
                        entered_gradient: style.getGradientEntered()

                        onMouseAreaClicked: {
                            if (db.removeTuple({ category : haveDependentElement.category}, catsTable))
                            {
                                notes.selectCheckedCats(haveDependentElement.category)
                                selectSearchCategory.removeElement(haveDependentElement.category)
                                searchGridCategory.removeElement(haveDependentElement.category)
                                rightGrid.removeElementRight(haveDependentElement.index)
                                notes.removeCategory(haveDependentElement.notesWithCategory, haveDependentElement.category)

                                haveDependentElement.close()
                            }
                            else
                            {
                                error.log = qsTr("Error deleting database row") + settings.emptyString
                                error.err_name = qsTr("Error deleting database row: ") + settings.emptyString + db.getError();
                                error.open()
                            }

                        }
                    }
                    OtherModules.StylishButton {
                        id: not_ok
                        objectName: "notOkButton"
                        width: 80
                        Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                        button_name: qsTr("No") + settings.emptyString
                        background_gradient: style.getGrayGradientStandart()
                        entered_gradient: style.getGrayGradientEntered()

                        onMouseAreaClicked: {
                            haveDependentElement.close()
                        }
                    }

                }
            }
        }

        MouseArea {
            id: inputRectClosingMouseArea
            anchors {
                top: scrcol.top
                topMargin: -scrcol.anchors.topMargin
                //bottom: scrcol.bottom
                left: scrcol.left
                right: inputRect.left
            }
            height: notesWindow.height
            enabled: false
            visible: enabled
            onClicked: {
                inputRect.state = "hide"
                enabled = false
            }
            Rectangle {
                anchors.fill: parent
                color: "black"
                opacity: 0.2
            }
        }

        Item {

            id: inputRect
            //objectName: "inputRect"
            anchors.top: parent.top
            anchors.topMargin: -(scrcol.anchors.topMargin + 5)
            anchors.bottom: parent.bottom
            //anchors.bottomMargin: -10
            //anchors.left: lg.right
            //z: 100
            //  color: "#43346f"
            state: "hide"
            //anchors.topMargin: 105
            width: Math.min(notesWindow.width, notesWindow.height) / 7 * 4//grid.width
            //height: notes.height
            anchors.left: parent.right
            visible: false
            implicitWidth: notes.width
            implicitHeight: notes.height

            Timer {
                id: timerRightNavCloses
                interval: 250
                running: false
                //triggered: false
                onTriggered: {
                    running = false
                    inputRect.visible = false
                }
            }
            onStateChanged: {
                if (state === "show") {
                    inputRect.visible = true
                    inputRectClosingMouseArea.enabled = true
                } else
                {
                    timerRightNavCloses.running = true
                    inputRectClosingMouseArea.enabled = false
                }
            }

            states: State {
                name: "show"
                AnchorChanges {
                    target: inputRect
                    //anchors.left: lg.left
                    anchors.left: undefined
                    anchors.right: parent.right
                    //anchors.left: undefined
                    //anchors.horizontalCenter: search.horizontalCenter

                }
            }
            State {
                name: "hide"
                AnchorChanges {
                    target: inputRect
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.right: undefined
                    anchors.left: parent.right
                    //anchors.horizontalCenter: undefined
                    //anchors.left: lg.right
                }
            }
            transitions: Transition {
                // smoothly reanchor myRect and move into new position
                AnchorAnimation { duration: 200 }
            }

            Rectangle {
                anchors.left: parent.left
                anchors.verticalCenter: notes.verticalCenter
                // anchors.right: parent.right
                height: notesWindow.height + 10
                width: inputRect.width + inputRect.width / 3
                color: "black"
                opacity: 0.6
                //radius: 10
                z: -1
            }


            //  gradient: style.getBackgroundGradient()
            Column {

                id: col
                objectName: "col"
                anchors.fill: parent
                anchors.topMargin: 50
                anchors.leftMargin: 20
                anchors.rightMargin: 15

                // anchors.top: parent.top
                // anchors.topMargin: 60
                width: Math.min(notesWindow.width, notesWindow.height) / 8 * 4

                RightNavCategory {

                    id: rightGrid

                    width: parent.width
                    //width: search.width - 110
                    height: notesWindow.height / 5 * 4
                    textColor: style.getTextColor()
                    // anchors.top: parent.top
                    // anchors.topMargin: 60
                    //width: Math.min(notesWindow.width, notesWindow.height) / 8 * 3
                }
            }
        }

        Repeater{
            id: repeater
            model: 6
            delegate: OtherModules.GuideRectangle {
                id: guide_ism
                onPromptClicked: {
                    root.theWindow.setNotWinVisible(model.index)
                }


                /*states: [
                State {
                    name: "parentAddCategory"
                    ParentChange { target: guide_nw; parent: addCategoryItem; x: 10; y: 10 }
                },
                State {
                    name: "parentNewButton"
                    ParentChange { target: guide_nw; parent: notes; x: 10; y: 10 }
                },
                State {
                    name: "parentCreateNote"
                    ParentChange { target: guide_nw; parent: createNoteItem; x: 10; y: 10 }
            ]*/

                states: [
                    State {
                        name: "parentAddCategory"
                        ParentChange { target: guide_ism; parent: addCategoryItem; x: 0; y: colorGrid.height}
                    },
                    State {
                        name: "parentCreateNode"
                        ParentChange { target: guide_ism; parent: createNoteItem; x: createNoteSelectCategory.height; y: 0}
                    }
                ]

            }
            Component.onCompleted: {
                for (var i = 0; i < repeater.count; ++i){

                    if (i === 0){
                        repeater.itemAt(i).promptText = qsTr("To create a note press '+', below the delete button") + settings.emptyString
                        repeater.itemAt(i).parent = new_button
                        repeater.itemAt(i).anchors.bottom = new_button.top
                        repeater.itemAt(i).anchors.horizontalCenter = new_button.horizontalCenter
                    }
                    else if (i === 1){
                        repeater.itemAt(i).promptText = qsTr("To create a category, click 'create'") + settings.emptyString
                        repeater.itemAt(i).promptRotation = -90
                        repeater.itemAt(i).parent = createCategoryBtn
                        repeater.itemAt(i).anchors.right = createCategoryBtn.left
                        repeater.itemAt(i).anchors.verticalCenter = createCategoryBtn.verticalCenter
                    }
                    else if (i === 2){
                        repeater.itemAt(i).promptText = qsTr("Browse the list of categories, click 'more'") + settings.emptyString
                        repeater.itemAt(i).parent = catButton
                        repeater.itemAt(i).anchors.bottom = catButton.top
                        repeater.itemAt(i).anchors.horizontalCenter = catButton.horizontalCenter
                    }
                    else if (i === 3){
                        repeater.itemAt(i).promptText = qsTr("All created categories will appear here. To remove a category, hold down the left mouse button.") + settings.emptyString
                        repeater.itemAt(i).anchors.right = inputRect.right
                    }
                    else if (i === 4){
                        repeater.itemAt(i).promptText = qsTr("This is a category creation menu. Enter a name and choose a color") + settings.emptyString
                        repeater.itemAt(i).state = "parentAddCategory"
                    }
                    else if (i === 5){
                        repeater.itemAt(i).promptText = qsTr("Here you can select the created category, below you can write a note") + settings.emptyString
                        repeater.itemAt(i).state = "parentCreateNode"
                    }
                    changeGuideVisible()
                }
            }

            function changeGuideVisible(){
                for (var i = 0; i < repeater.count; ++i){
                    if (settings.getPromptMode()) {
                        repeater.itemAt(i).doUpdate(root.theWindow.getNotWinVisible(i))
                    } else {
                        repeater.itemAt(i).doUpdate(false)
                    }
                }
            }
        }

        Loader {
            id: errorLoader
        }

        Component {
            id: errorComponent
            OtherModules.ErrorDialog {
                id: error
                visible: visibleErrorDialog
                log: log
                err_name: errName
            }
        }
        Item
        {
            id: root
            property Window theWindow: Window.window
            Connections
            {
                target: root.theWindow
                onShowPromptChanged: {
                    repeater.changeGuideVisible()
                }
            }
        }
    }
    StyleModules.Style {
        id: style
    }
    /** @brief Видимость окна ошибок */
    property var visibleErrorDialog : false
    /** @brief Лог ошибки*/
    property var log : ""
    /** @brief Текст ошибки*/
    property var errName: ""

    property string selectFileText: qsTr("Select a file") + settings.emptyString
    /** @brief Видимость подсказок*/
    property bool guide_visible: guide.visible
}

