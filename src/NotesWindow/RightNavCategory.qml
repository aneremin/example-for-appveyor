/**
  * @file
  * @brief Грид категорий (правая панель)
  *
*/
import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0

/*!
    \class RightNavCategory
    \brief Грид категорий (правая панель)
 */
MouseArea {

    /** @brief Флаг возможности добавления новых элементов */
    property bool editSearchCategory: true
    height: 30

    property string textColor
    propagateComposedEvents: true

    GridView {
        id: catgrid

        anchors.fill: parent
        anchors.margins: 5
        //anchors.leftMargin: 10
        //anchors.rightMargin: 10
        height: parent.height
        clip: true
        cellWidth: cellHeight * 2.9
        cellHeight: 35
        //focus: true
        //interactive: true
        //flow: undefined
        //layoutDirection: Qt.LeftToRight
        //verticalLayoutDirection: Grid.TopToBottom
        //snapMode: GridView.SnapToRow
        flickableDirection: Flickable.VerticalFlick

        delegate: component2Category
        model: listModelRight

        onCountChanged: {
            if (count > 0)
                emptyText.visible = false
            else
                emptyText.visible = true
        }

        Text {
            id: emptyText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("Here is empty") + settings.emptyString
            color: "white"
            font.pixelSize: 15
            visible: true
            opacity: 0.5
            z: 5
        }
    }

    ListModel
    {
        id: listModelRight
        Component.onCompleted: {

            var list = db.selectAllTuples(2, catsTable)

            var i = 1
            if (list[0] > 1)
            {
                for (; i < list[0];) {
                    append({cat: list[i], colorCat: list[i + 1],checked1 : false})

                    i = i + 2
                }
            }
        }
    }

    /** @brief Добавление элемента в грид - боковую панель */
    function addingElementRight()
    {
        listModelRight.append({cat: cateEdit.text, colorCat: colorGrid.getCurrentColor(),checked1 : false})
    }

    /** @brief Получение цвета элемента из грида - боковой панели */
    function getColor(cat) {
        for (let i = 0; i < listModelRight.count;i++)
        {
            if (listModelRight.get(i).cat === cat)
            {
                return listModelRight.get(i).colorCat
            }
        }
        return "#000000"
    }

    /** @brief Удаление элемента из грида - боковой панели */
    function removeElementRight(indx) {
        listModelRight.remove(indx)
    }

    /** @brief Активация чекбоксов всех категорий из categories */
    function makeChecked(categories) {

        var has
        for (let k = 0; k < listModelRight.count;k++)
        {
            has = false
            for (var j in categories)
            {
                if (db.toLower(categories[j]) === db.toLower(listModelRight.get(k).cat))
                {
                    listModelRight.get(k).checked1 = true
                    has = true
                    break
                }
            }
            if (!has)
                listModelRight.get(k).checked1 = false
        }


    }

    /** @brief Деактивация чекбоксов категорий == category */
    function removeChecked(category) {


        for (let k = 0; k < listModelRight.count;k++)
        {
                if (db.toLower(category) === db.toLower(listModelRight.get(k).cat))
                {
                    listModelRight.get(k).checked1 = false

                    break
                }
        }


    }


    Component {
        id: component2Category

        DelayButton {
            id: delayBtn
            width: catgrid.cellWidth - 7
            height: catgrid.cellHeight - 3
            delay: 500

            //color: colorCat
            //radius: 5
            background: Rectangle {
                color: "transparent"
            }

            onActivated: {

                var notesLst = notes.findRemovedCategory(listModelRight.get(index).cat)
                if (notesLst.length !== 0)
                {
                    haveDependentElement.index = index
                    haveDependentElement.category = listModelRight.get(index).cat
                    haveDependentElement.notesWithCategory = notesLst
                    haveDependentElement.open()
                }
                else
                {
                    if (db.removeTuple({ category : listModelRight.get(index).cat}, catsTable))
                    {
                        notes.selectCheckedCats(listModelRight.get(index).cat)
                        selectSearchCategory.removeElement(listModelRight.get(index).cat)
                        searchGridCategory.removeElement(listModelRight.get(index).cat)
                        rightGrid.removeElementRight(index)
                        notes.removeCategory(notesLst, listModelRight.get(index).cat)

                    }
                    else
                    {
                        error.log = qsTr("Error deleting database row") + settings.emptyString
                        error.err_name = qsTr("Error deleting database row: ") + settings.emptyString + db.getError();
                        error.open()
                    }
                }
            }

            indicator: Rectangle {
                width: parent.width
                height: parent.height
                color: colorCat
                opacity: 1 - delayBtn.progress
                radius: 5
                TextArea{
                    id: catText
                    anchors.top: parent.top
                    anchors.left: parent.left
                    width: parent.width - 30
                    height: parent.height
                    text: cat.substr(0,8) + ((cat.length > 8) ? "..." : "")
                    // anchors.horizontalCenter: parent.horizontalCenter
                    clip: false

                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    enabled: false

                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 9
                    color: "white"

                    // ScrollBar.vertical: null
                }

                CheckBox {
                    id: check
                    checked: (checked1) ? true : false
                    scale: 0.5
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    //anchors.rightMargin: 5
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    indicator: Rectangle {
                        implicitWidth: 26
                        implicitHeight: 26
                        color:"white"
                        opacity: 0.7
                        Rectangle {
                            width: 14
                            height: 14
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            color: "black"
                            opacity: 0.8
                            visible: check.checked
                        }
                    }
                    onCheckedChanged: {
                        if (checked)
                        {
                            listModelRight.get(index).checked1 = true
                            if (editSearchCategory)
                            searchGridCategory.addingElement(listModelRight.get(index).cat,
                                                             listModelRight.get(index).colorCat)
                        }
                        else
                        {
                            listModelRight.get(index).checked1 = false
                            if (editSearchCategory)
                            searchGridCategory.removeElement(listModelRight.get(index).cat)
                        }
                    }
                }
            }

        }

    }

}
