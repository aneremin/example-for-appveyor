/**
  * @file
  * @brief Грид категорий (полоска под строкой поиска)
  *
*/
import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0

/*!
    \class SearchCategory
    \brief Грид категорий (полоска под строкой поиска)
 */
MouseArea {


    height: 30
    onWheel: {
        //scrollBar.increase()
        catgrid.flick(-wheel.angleDelta.y * 4,-wheel.angleDelta.y * 4)
    }
    propagateComposedEvents: true

    GridView {
        id: catgrid

        anchors.fill: parent
        anchors.margins: 5
        //anchors.leftMargin: 10
        //anchors.rightMargin: 10
        height: parent.height
        clip: true
        cellWidth: cellHeight * 2.9
        cellHeight: height
        //focus: true
        //interactive: true
        flow: GridView.FlowTopToBottom
        //layoutDirection: Qt.LeftToRight
        //verticalLayoutDirection: Grid.TopToBottom
        //snapMode: GridView.SnapToRow
        flickableDirection: Flickable.HorizontalFlick

        delegate: componentCategory
        model: listModel
    }

    ListModel
    {
        id: listModel
        onCountChanged: {
            notesWindow.countOfCategoriesChanged()
        }
    }

    /** @brief Получение списка всех категорий из грида под поисковым полем */
    function getAllCategories() {
        var list = ""

        for (let i = 0; i < listModel.count;i++)
        {

                list += listModel.get(i).cat + ((i === listModel.count - 1) ? "" : ",")

        }
        return db.toList(list)
    }

    /** @brief Добавление элемента в грид под поисковым полем */
    function addingElement(cat, colorCat)
    {
        listModel.append({cat: cat, colorCat: colorCat})
    }

    /** @brief Удаление элемента из грида под поисковым полем */
    function removeElement(cat)
    {
        for (let i = 0; i < listModel.count;i++)
        {
            if (listModel.get(i).cat === cat)
            {
                listModel.remove(i)
                break
            }
        }
    }

    /** @brief Добавление введенных в поисковой строке категорий */
    function findByName(categories)
    {
            var has = false
            var color = ""
            for (var i in categories) {
                color = selectSearchCategory.findCategory(categories[i])
                if (color === "")
                    continue

                has = false
                for (let j = 0; j < listModel.count; j++)
                {
                    if (db.toLower(listModel.get(j).cat) === db.toLower(categories[i]))
                    {
                        has = true
                        break
                    }
                }
                if (!has)
                {
                    listModel.append({cat: categories[i],
                                             colorCat: color})
                }

            }
    }



    Component {
        id: componentCategory

        Rectangle {
            width: catgrid.cellWidth - 7
            height: catgrid.cellHeight - 3
            color: colorCat
            radius: 5

            Flickable {
                id: catText
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.right: closeCat.left
                flickableDirection: Flickable.HorizontalFlick
                interactive: true
                // ScrollBar.horizontal: ScrollBar {}

                TextArea.flickable: TextArea{

                    text: cat
                    // anchors.horizontalCenter: parent.horizontalCenter
                    clip: true
                    anchors.fill: parent

                    horizontalAlignment: Text.AlignLeft
                    //verticalAlignment: Text.AlignTop
                    enabled: false

                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 7
                    color: "white"

                    // ScrollBar.vertical: null
                }
            }
            Rectangle{
                id: closeCat
                anchors.right: parent.right
                //anchors.rightMargin: 5
                anchors.top: parent.top
                anchors.topMargin: 3
                height: parent.height - 5
                width: height
                opacity: 0.6
                scale: 0.7
                color: "transparent"//"#e06971"//"#30313A"//gradient: style.getGradientStandart()
                Image {
                    anchors.fill: parent
                    anchors.margins: 2

                    id: close_img
                    source: "/resources/img/close.png"
                }
                states:[
                    State {
                        name: "BUTTON_ENTERED"
                        PropertyChanges { target: closeCat; gradient: style.getAttentionGradientStandart()}
                    },
                    State {
                        name: "BUTTON_EXITED"
                        PropertyChanges { target: closeCat; color: "transparent"}
                    }
                ]
                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: closeCat.state = "BUTTON_ENTERED"
                    onExited:  closeCat.state = "BUTTON_EXITED"
                    onClicked: {
                        rightGrid.removeChecked(listModel.get(index).cat)
                        listModel.remove(index)
                    }
                }
            }
        }

    }

}
