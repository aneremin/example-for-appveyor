/**
  * @file
  * @brief Приветственное окно
  *
  * Окно с описанием приложения и соглашением для старта работы
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.3
import "../Other" as OtherModules
import "../Style" as StyleModules

/*!
    \class StartWindow
    \brief Приветственное окно
 */
Window {
    id: start_window
    width: Screen.width * 0.61
    height: Screen.height * 0.65

    x: Screen.width / 2 - width / 2
    y: Screen.height / 2 - height / 2

    flags: Qt.FramelessWindowHint | Qt.Window

    /** @brief Координата X до перемещения мыши */
    property var prevX
    /** @brief Координата Y до перемещения мыши */
    property var prevY

    MouseArea{
        anchors.fill: parent
        onPressed: {
            prevX = mouseX
            prevY = mouseY
        }

        onMouseXChanged: {
            var dx = mouseX - prevX
            start_window.setX(start_window.x + dx)
        }

        onMouseYChanged: {
            var dy = mouseY - prevY
            start_window.setY(start_window.y + dy)
        }
    }

    LinearGradient {
        id: lg
        anchors.fill: parent
        anchors.topMargin: 0
        start: Qt.point(0,0)
        end: Qt.point(parent.width, parent.height)

        gradient: Gradient{
            id: backgroundGradient
            GradientStop {
                position: 0
                color: "#3C4653"

                SequentialAnimation on color {
                    id: colorAnimation
                    running: true
                    loops: Animation.Infinite
                    ColorAnimation { to: "#493DA0" }
                    ColorAnimation { to: "#5C4653"; duration: 3000 }
                    ColorAnimation { to: "#773143"; duration: 3000 }
                    ColorAnimation { to: "#771E36"; duration: 3000 }
                    ColorAnimation { to: "#771E4D"; duration: 3000 }
                    ColorAnimation { to: "#770140"; duration: 3000 }
                    ColorAnimation { to: "#771E4D"; duration: 3000 }
                    ColorAnimation { to: "#771E36"; duration: 3000 }
                    ColorAnimation { to: "#773143"; duration: 3000 }
                    ColorAnimation { to: "#5C4653"; duration: 3000 }
                    ColorAnimation { to: "#493DA0"}
                }

            }
            GradientStop {
                position: 1
                color: "#30313A"
                SequentialAnimation on color {
                    id: colorAnimation2
                    running: true
                    loops: Animation.Infinite
                    ColorAnimation { to: "#30123A"}
                    ColorAnimation { to: "#301262"; duration: 3000 }
                    ColorAnimation { to: "#22264A"; duration: 3000 }
                    ColorAnimation { to: "#1B2050"; duration: 3000 }
                    ColorAnimation { to: "#4A2050"; duration: 3000 }
                    ColorAnimation { to: "#712050"; duration: 3000 }
                    ColorAnimation { to: "#4A2050"; duration: 3000 }
                    ColorAnimation { to: "#1B2050"; duration: 3000 }
                    ColorAnimation { to: "#22264A"; duration: 3000 }
                    ColorAnimation { to: "#301262"; duration: 3000 }
                    ColorAnimation { to: "#30123A";}
                }

            }
        }
    }

    Rectangle {
        id: language_selection
        //source: "/resources/img/ru.png"

        anchors.right: parent.right
        anchors.top: parent.top
        anchors.rightMargin: 10
        anchors.topMargin: 10

        height: parent.width / 23
        width: parent.width / 23

        radius: width / 2

        color: "#fefefe"
        opacity: 0.5

        Text {
            id: lang
            text: qsTr("ENG")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            //color: "#ffffff"
        }

        MouseArea{
            hoverEnabled: true

            anchors.fill: parent
            onClicked: {
                var language = "en"
                if (lang.text == "ENG"){
                    lang.text = qsTr("RU")
                    language = "ru"
                }
                else{
                    lang.text = qsTr("ENG")
                    language = "en"
                }
                settings.setActiveLanguage(language)
                settings.selectLanguage(language)
            }

            onEntered: {
                maximize.start()
            }

            onExited: {
                minimize.start()
            }
        }
    }

    ParallelAnimation {
        id: maximize
        running: false
        PropertyAnimation {target: language_selection; property: "width";  to: start_window.width / 23 + 3; duration: 200}
        PropertyAnimation {target: language_selection; property: "height";  to: start_window.width / 23 + 3; duration: 200}
    }

    ParallelAnimation {
        id: minimize
        running: false
        PropertyAnimation {target: language_selection; property: "width";  to: start_window.width / 23 - 3; duration: 200}
        PropertyAnimation {target: language_selection; property: "height";  to: start_window.width / 23 - 3; duration: 200}
    }

    SwipeView{
        id: guideList
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width / 1.2
        height: parent.height / 1.3
        spacing: 70
        clip: false

        Item{
            id: firstPage

            AnimatedImage {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                width: guideList.width / 1.1
                height: guideList.height

                //anchors.fill: parent
                source: "/resources/img/animated_logo.gif"

                opacity: 0
                SequentialAnimation on opacity {
                    id: aposAnimation
                    running: true
                    loops: 1
                    PropertyAnimation { to: 1; duration: 2500 }
                }
            }
        }

        Item{
            id: secondPage
            Rectangle{
                id: secondtxt
                width: guideList.width
                height: 30
                color: "black"
                opacity: 0.3
                Text{
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Upload and put your character on the main window") + settings.emptyString
                    color: "white"
                    font.pixelSize: Screen.height / 43.2

                }
            }

            Image {
                anchors.top: secondtxt.bottom
                anchors.topMargin: 3
                width: guideList.width
                height: guideList.height / 1.1
                //anchors.fill: parent
                source: "/resources/img/screen1.PNG"
            }
        }

        Item{
            id: thirdPage
            Rectangle{
                id: thirdtxt
                width: guideList.width
                height: 30
                color: "black"
                opacity: 0.3
                Text{
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Set reminders for any day so you don’t forget about important events") + settings.emptyString
                    color: "white"
                    font.pixelSize: Screen.height / 43.2
                }
            }

            Image {
                anchors.top: thirdtxt.bottom
                anchors.topMargin: 3
                width: guideList.width
                height: guideList.height / 1.1
                //anchors.fill: parent
                source: "/resources/img/screen3.PNG"
            }
        }

        Item{
            id: fourthPage
            Rectangle{
                id: fourthtxt
                width: guideList.width
                height: 30
                color: "black"
                opacity: 0.3
                Text{
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Create your own comands and get quick access to them using shortcuts or voice input") + settings.emptyString
                    color: "white"
                    font.pixelSize: Screen.height / 43.2
                }
            }

            Image {
                anchors.top: fourthtxt.bottom
                anchors.topMargin: 3
                width: guideList.width
                height: guideList.height / 1.1
                //anchors.fill: parent
                source: "/resources/img/screen2.PNG"
            }
        }



        Item{
            id: fifthPage
            Rectangle{
                id: fifthtxt
                width: guideList.width
                height: 30
                color: "black"
                opacity: 0.3
                Text{
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Create notes and categorize them") + settings.emptyString
                    color: "white"
                    font.pixelSize: Screen.height / 43.2
                }
            }
            Image {
                anchors.top: fifthtxt.bottom
                anchors.topMargin: 3
                width: guideList.width
                height: guideList.height / 1.1
                //anchors.fill: parent
                source: "/resources/img/screen4.PNG"
            }
        }
    }

    Row{
        anchors.top: guideList.bottom
        anchors.topMargin: 3
        anchors.horizontalCenter: parent.horizontalCenter
        width: left_arrow.width + control.width + right_arrow.width

        Image {
            id: left_arrow
            anchors.verticalCenter: parent.verticalCenter
            width: 30
            height: 30
            source: "/resources/img/arrow_left.png"

            MouseArea{
                anchors.fill: parent
                hoverEnabled: true

                onEntered: {width_maximize.start(); height_maximize.start()}

                onExited: {width_minimize.start(); height_minimize.start()}

                onClicked: {
                    if (guideList.currentIndex != 0){
                        guideList.currentIndex -= 1
                    }
                }
            }

            SequentialAnimation on width {
                id: width_maximize
                running: false
                PropertyAnimation { to: 50; duration: 500}
            }

            SequentialAnimation on height {
                id: height_maximize
                running: false
                PropertyAnimation { to: 50; duration: 500}
            }

            SequentialAnimation on width {
                id: width_minimize
                running: false
                PropertyAnimation { to: 30; duration: 500}
            }

            SequentialAnimation on height {
                id: height_minimize
                running: false
                PropertyAnimation { to: 30; duration: 500}
            }
        }

        PageIndicator {
            id: control

            anchors.verticalCenter: parent.verticalCenter
            anchors.left: left_arrow.right

            count: guideList.count
            currentIndex: guideList.currentIndex

            interactive: true

            delegate: Rectangle {
                implicitWidth: 12
                implicitHeight: 12

                radius: width / 2
                color: "#7D3BFF"

                opacity: index === control.currentIndex ? 0.95 : pressed ? 0.7 : 0.45

                Behavior on opacity {
                    OpacityAnimator {
                        duration: 100
                    }
                }
            }
            property bool flag: false
            onCurrentIndexChanged: {
                guideList.currentIndex = control.currentIndex
                if (currentIndex == count - 1){
                    flag = true
                    apearenceAnimation.start()
                }
                else{
                    if (flag){
                        disapearenceAnimation.start()
                        flag = false
                    }
                }
            }
        }

        Image {
            id: right_arrow
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: control.right
            width: 30
            height: 30
            source: "/resources/img/arrow_right.png"

            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {width_maximize2.start(); height_maximize2.start()}

                onExited: {width_minimize2.start(); height_minimize2.start()}

                onClicked: {
                    if (guideList.currentIndex != guideList.count - 1){
                        guideList.currentIndex += 1
                    }
                }
            }

            SequentialAnimation on width {
                id: width_maximize2
                running: false
                PropertyAnimation { to: 50; duration: 500}
            }

            SequentialAnimation on height {
                id: height_maximize2
                running: false
                PropertyAnimation { to: 50; duration: 500}
            }

            SequentialAnimation on width {
                id: width_minimize2
                running: false
                PropertyAnimation { to: 30; duration: 500}
            }

            SequentialAnimation on height {
                id: height_minimize2
                running: false
                PropertyAnimation { to: 30; duration: 500}
            }
        }
    }

    Row{
        id:ok_row
        anchors.horizontalCenter: parent.horizontalCenter
        height: 30
        width: parent.width / 3
        y: start_window.height
        spacing: 10
        SequentialAnimation on y {
            id: apearenceAnimation
            running: false
            loops: 1
            PropertyAnimation { to: start_window.height - ok_row.height; duration: 500 }
        }

        SequentialAnimation on y {
            id: disapearenceAnimation
            running: false
            loops: 1
            PropertyAnimation { to: start_window.height; duration: 500 }
        }

        Rectangle{
            anchors.fill: parent
            color: "black"
            opacity: 0.3
        }

        CheckBox {
            id: accept_box
            text: qsTr("Accept agreement") + settings.emptyString
            width: parent.width * 0.5
            checked: false

            indicator: Rectangle {
                implicitWidth: Screen.height / 41.53
                implicitHeight: Screen.height / 41.53
                x: accept_box.leftPadding
                y: parent.height / 2 - height / 2
                radius: 3
                border.color: accept_box.down ? "#381874" : "#4E15B5"

                Rectangle {
                    width: Screen.height / 77.14
                    height: Screen.height / 77.14
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    radius: 10
                    color: accept_box.down ? "#381874" : "#4E15B5"
                    visible: accept_box.checked
                }
            }

            contentItem: Text {
                text: accept_box.text
                //font: accept_box.font
                opacity: enabled ? 1.0 : 0.3
                color: accept_box.down ? "#381874" : "white"
                verticalAlignment: Text.AlignVCenter
                leftPadding: accept_box.indicator.width + accept_box.spacing
                font.pixelSize: 12
            }
        }

        OtherModules.StylishButton{
            id: begin_button
            //width: parent.width * 0.5 -
            anchors.left: accept_box.right
            anchors.leftMargin: accept_box.indicator.width + accept_box.spacing
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 5
            enabled: accept_box.checked
            height: 20
            button_name: qsTr("Begin") + settings.emptyString
            //background_gradient: style.getGradientStandart()
            entered_gradient: style.getGradientEntered()
            onMouseAreaClicked: {
                if (accept_box.checked){
                    start_window.close();
                    settings.setStartWindowViewed(true);
                    settings.generateSettings();
                }
            }
            background_gradient: Gradient{
                id: backgrdGradient
                GradientStop {
                    position: 0
                    color: "#6C669D"

                    SequentialAnimation on color {
                        id: colrAnimation
                        running: true
                        loops: Animation.Infinite
                        ColorAnimation { to: "#F5203E"; duration: 3000 }
                        ColorAnimation { to: "#6C669D"; duration: 3000 }
                    }

                }
                GradientStop {
                    position: 1
                    color: "#6534AC"
                    SequentialAnimation on color {
                        id: colrAnimation2
                        running: true
                        loops: Animation.Infinite
                        ColorAnimation { to: "#B2172C"; duration: 3000 }
                        ColorAnimation { to: "#6534AC"; duration: 3000 }
                    }

                }
            }
        }
    }

    Shortcut {
        sequences: ["Right"]
        enabled: true
        onActivated: {
            if (guideList.currentIndex != guideList.count - 1){
                guideList.currentIndex += 1
            }
        }
    }

    Shortcut {
        sequences: ["Left"]
        enabled: true
        onActivated: {
            if (guideList.currentIndex != 0){
                guideList.currentIndex -= 1
            }
        }
    }


    StyleModules.Style{
        id: style
    }
}
