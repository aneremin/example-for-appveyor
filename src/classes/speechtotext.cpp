/*!
 * \file
 * \brief Реализация конвертации звуковой информации в текстовую
 *
 * Запись звука, его побайтовая запись в запрос, отправка запроса, получение ответа, вывод результата
 */

#include "../../include/speechtotext.h"

/*!
 * \brief Конструктор класса SpeechToText
 *
 * Инициализирует экземпляр классов для отправки запроса и записи звука
 * (QNetworkAccessManager и QAudioRecorder), а также позволяет настроить параметры ввода звука
 */
SpeechToText::SpeechToText(QString shortcutAppActivation, QString shortcutFastCommandsActivation)
{

    m_manager = new QNetworkAccessManager(); // Инициализация классов для отправки запроса и записи звука
    m_audioRecorder = new QAudioRecorder();
    isRunning = false;
    evenClick = true;

#ifdef Q_OS_WIN //Прописывание параметров диктофона для разных ОС
    container = ""; // audio/x-wav

    settings.setCodec(""); // audio/pcm
    settings.setSampleRate(0); // 0
    settings.setBitRate(0); // 32000
    settings.setChannelCount(1); // 1
    settings.setQuality(QMultimedia::EncodingQuality(16000));
    settings.setEncodingMode(QMultimedia::ConstantQualityEncoding);
#else
    #ifdef Q_OS_LINUX
    container = "audio/x-wav";

    settings.setCodec("audio/x-mulaw");
    settings.setSampleRate(0);
    settings.setBitRate(32000);
    settings.setChannelCount(1);
    settings.setQuality(QMultimedia::EncodingQuality(16000));
    settings.setEncodingMode(QMultimedia::ConstantBitRateEncoding);
    #endif
    // другая ОС не поддерживается
#endif

    connect( m_manager, SIGNAL( finished( QNetworkReply* ) ), SLOT( onResponse( QNetworkReply* ) ) );// Вызов принимающей функции после ответа от сервера
    //connect(m_manager, SIGNAL(error(QNetworkReply::NetworkError)),
            //this, SLOT(setErrorText(QNetworkReply::NetworkError)));
    connect(m_audioRecorder, QOverload<QMediaRecorder::Error>::of(&QAudioRecorder::error), this,
            &SpeechToText::displayErrorMessage);

    QxtGlobalShortcut* qxtshortcut = new QxtGlobalShortcut(QKeySequence("Ctrl+Alt+S"));
    connect(qxtshortcut, &QxtGlobalShortcut::activated, [=]() {
        emit changeRecording();
    });

    pshortcutAppActivation = new QxtGlobalShortcut(QKeySequence(shortcutAppActivation));
    connect(pshortcutAppActivation, &QxtGlobalShortcut::activated, [=]() {
        emit showApp();
    });
    pshortcutFastCommandsActivation = new QxtGlobalShortcut(QKeySequence(shortcutFastCommandsActivation));
    connect(pshortcutFastCommandsActivation, &QxtGlobalShortcut::activated, [=]() {
        emit showFastCommands();
    });
    //setGlobalShortcuts(list);
    // информация о поддерживаемом openssl
    qDebug() << QSslSocket::sslLibraryVersionString();
}

void SpeechToText::setShortcutAppActivation(QString shortcut)
{
    disconnect(pshortcutAppActivation, &QxtGlobalShortcut::activated, nullptr, nullptr);
    delete pshortcutAppActivation;
    pshortcutAppActivation = new QxtGlobalShortcut(QKeySequence(shortcut));
    connect(pshortcutAppActivation, &QxtGlobalShortcut::activated, [=]() {
        emit showApp();
    });
}

void SpeechToText::setShortcutFastCommandsActivation(QString shortcut)
{
    disconnect(pshortcutFastCommandsActivation, &QxtGlobalShortcut::activated, nullptr, nullptr);
    delete pshortcutFastCommandsActivation;
    pshortcutFastCommandsActivation = new QxtGlobalShortcut(QKeySequence(shortcut));
    connect(pshortcutFastCommandsActivation, &QxtGlobalShortcut::activated, [=]() {
        emit showFastCommands();
    });
}

/*!
 * \brief Получение текстового представления нажатого сочетания клавиш
 * \return Текст сочетания
 */
QString SpeechToText::getGlobalShortcut()
{
    return rezultShortcut;
}

/*!
 * \brief Инициализация сочетаний клавиш
 * \param list список сочетаний клавиш
 */
void SpeechToText::setGlobalShortcuts(QStringList list)
{
    if (qxtshortcuts)
    {
        for (i = 0; i < shortcutsCount; i++)
        {
            disconnect(qxtshortcuts[i], &QxtGlobalShortcut::activated, nullptr, nullptr);
            delete qxtshortcuts[i];
            qDebug() << "delete";
        }
        delete qxtshortcuts;
    }
    qDebug() << list.length();
    shortcutsCount = list.length();
    qxtshortcuts = new QxtGlobalShortcut*[shortcutsCount];
    for (i = 0; i < shortcutsCount; i++)
    {

        qDebug() << list.at(i);
        qxtshortcuts[i] = new QxtGlobalShortcut(QKeySequence(list.at(i)));
        qDebug() << "add";
    }
    //qxtshortcut->setShortcut(QKeySequence(tr("Ctrl+X")));

    for (i = 0; i < shortcutsCount; i++)
    {
        connectShortcuts(i);
    }



}

/*!
 * \brief Соединение сочетаний клавиш (QxtGlobalShortcut) с объектом
 * \param j индекс сочетания
 */
void SpeechToText::connectShortcuts(int j)
{
    connect(qxtshortcuts[j], &QxtGlobalShortcut::activated, [=]() {
        rezultShortcut = qxtshortcuts[j]->shortcut().toString();
        qDebug() << rezultShortcut;
        emit processGlobalShortcut();
    });
}

/*!
 * \brief Деструктор класса SpeechToText
 *
 * Освобождает выделенную ранее память
 */
SpeechToText::~SpeechToText()
{
    delete m_manager;
    delete m_audioRecorder;
    if (qxtshortcuts)
    {
        for (i = 0; i < shortcutsCount; i++)
        {
            disconnect(qxtshortcuts[i], &QxtGlobalShortcut::activated, nullptr, nullptr);
            delete qxtshortcuts[i];
            qDebug() << "delete";
        }
        delete qxtshortcuts;
    }
}

/*!
 * \brief Отправка POST запроса на сервер wit.ai
 *
 * Происходит отправка запроса с указанием url сервера, key, общих параметров
 */
void SpeechToText::onSend()
{

    QByteArray key = "GI4OPK3YOIVIEND4PJINDS6HMMZXAFUC";
    QUrl url("https://api.wit.ai/speech?v=20170307");

    QNetworkRequest request(url);
    request.setRawHeader("Authorization", "Bearer " + key);
    request.setRawHeader("Content-Type", "audio/wav");


    QFile file(pathAudioFile); // Открытие файла со звуком для последующего преобразования в набор байт
    file.open(QIODevice::ReadOnly);
    QByteArray data = file.readAll();

    qDebug() << QSslSocket::supportsSsl() << QSslSocket::sslLibraryBuildVersionString() << QSslSocket::sslLibraryVersionString();



    m_manager->post(request,data);// Отправка POST запроса

}

/*!
 * \brief Получение ответа от сервера
 * \param reply переменная, в которой хранится JSON-ответ сервера
 *
 * Происходит запись в переменную rezultText ответа сервера и отправка
 * сигнала, который "говорит" о пришедшем ответе
 */
void SpeechToText::onResponse(QNetworkReply* reply)
{

    rezultText = QJsonDocument::fromJson(reply->readAll()).object().value("_text").toString();

    emit sendTexttoMainClass();

}

/*!
 * \brief Запись звука
 *
 * Включение/выключение диктофона для записи/прерывания звука
 */
void SpeechToText::toggleRecord()
{
    isRunning = true;
    evenClick = (evenClick) ? false : true;

    pathAudioFile = QDir::currentPath() + "/sample.wav";

    if (m_audioRecorder->state() == QMediaRecorder::StoppedState) {

#ifdef Q_OS_WIN // Настройка диктофона для разных ОС
        m_audioRecorder->setAudioInput("");
#else
        m_audioRecorder->setAudioInput("default:");
#endif
        m_audioRecorder->setEncodingSettings(settings, QVideoEncoderSettings(), container);
        m_audioRecorder->setOutputLocation(QUrl::fromLocalFile(pathAudioFile));

        m_audioRecorder->record(); // Начало записи звука с микрофона


    }
    else {

        m_audioRecorder->stop();// Остановка записи звука с микрофона
        emit secondClick();
        onSend(); // Отправка полученного звука
                 // При остановке записи происходит вызов функции отправки POST запроса
    }
}

/*!
 * \brief Получение результата запроса
 * \return Обработанные данные
 */
QString SpeechToText::getRezult()
{
        return rezultText;
}

/*!
 * \brief Изменение флага работы над записью/ожиданием записи
 * \param flag Индикатор работы функций
 *
 * flag == true - идет запись звука/ожидание ответа от сервера
 *
 * flag == false - иначе
 */
void SpeechToText::setRunning(bool flag)
{
        isRunning = flag;
}

/*!
 * \brief Получение текущего состояния преобразующего процесса
 * \return Состояние процесса: true - процесс идет, false - иначе
 */
bool SpeechToText::getRunning()
{
        return isRunning;
}

/*!
 * \brief Вывод сообщения об ошибках в терминале, отправка сигнала ошибки
 */
void SpeechToText::displayErrorMessage()
{
    qDebug() << "Error:" << m_audioRecorder->errorString();
    errorText = m_audioRecorder->errorString();
    emit recordError();
}

/*!
 * \brief Получить текст ошибки
 * \return errorText
 */
QString SpeechToText::getErrorText()
{
    return errorText;
}

void SpeechToText::setErrorText(QString text)
{
    errorText = text + "\n" + QCoreApplication::tr("To work with voice input you need to download ") +  QSslSocket::supportsSsl() + " " + QSslSocket::sslLibraryBuildVersionString() + " " + QSslSocket::sslLibraryVersionString();
    emit postError();
}

/*!
 * \brief Проверка подключения к интернету
 * \return Есть подключение / нет подключения
 */
bool SpeechToText::isInternetAccess()
{
    QProcess qp;
#ifdef Q_OS_WIN
    int exitCode = qp.execute("ping -n 1 198.41.0.4");
#else
    int exitCode = qp.execute("ping -c 1 198.41.0.4");
#endif
    if (exitCode==0) {
    return true;
    } else {
    return false;
    }
}

/*!
 * \brief Получение четности клика
 * \details Это нужно, чтобы ограничить нажатие пользователем кнопки записи голоса
 *          логика позволяет отлавливать 2 клика и далее блокировать запись
 * \return Четен / нечетен
 */
bool SpeechToText::getEvenClick()
{
    return evenClick;
}

/*!
 * \brief Выполнение действия (открытие приложений и тд)
 * \param action команда для действия в строком варианте
 * \return true
 */
bool SpeechToText::evecuteAction(QString action)
{
    QProcess qp;

#ifdef Q_OS_WIN
    qp.start("cmd /C " + action);

    qDebug() << action;

    if(qp.waitForStarted()){
        qDebug() << "Starting";
    }
    qp.waitForFinished(-1);
        qDebug() << "finish";
#else
    /*qp.setProgram(action);
    qp.start(action);
    //qp.waitForBytesWritten();
    if (!qp.waitForFinished(1)) {
    //qp.kill();
    qp.waitForFinished(-1);
    }


    int exitCode = qp.execute(action);

    if (exit Code==0)
    return true;*/
    threads.append(new QThread());
    actions.append(new Action());
    actions.last()->moveToThread(threads.last());
    connect(threads.last(), SIGNAL(started()), actions.last(), SLOT(doAction()));
    connect(actions.last(), &Action::workFinished, [=]() {
        threads.last()->quit();
        emit workFinished();
    });

    actions.last()->setAction(action);
    threads.last()->start();

    //connect(actions.last(), SIGNAL(workFinished()), actions.last(), SLOT(deleteLater()));
    //connect(threads.last(), SIGNAL(finished()), threads.last(), SLOT(deleteLater()));
#endif
    return true;
}

/*!
 * \brief Проверка на ОС Windows
 * \return текущая ОС - Windows / текущая ОС не Windows
 */
bool SpeechToText::isWin()
{
#ifdef Q_OS_WIN
   return true;
#else
   return false;
#endif
}


