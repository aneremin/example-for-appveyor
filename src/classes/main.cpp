/*!
 * \file
 * \brief Точка входа в программу
 *
 * Инициализация объектов, вызов функций для передачи управления
 */

#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QFontDatabase>
#include "include/speechtotext.h"
#include "include/database.h"
#include "include/levenshtein.h"
#include "include/settings.h"

/*!
 * \brief Точка входа в программу
 * \param argc Кол-во переданных параметров
 * \param argv Массив строк переданных параметров
 * \return Код завершения функции
 */
int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling); // Позволяет автоматически масштабировать интерфейс

    QGuiApplication app(argc, argv);
    QFontDatabase::addApplicationFont(":/fonts/Cipitillo.otf");

    //QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
    //QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL);
    //QCoreApplication::setAttribute(Qt::AA_UseDesktopOpenGL);
    QCoreApplication::setAttribute(Qt::AA_NativeWindows);

    app.setOrganizationName("DeskHelper");
    app.setOrganizationDomain("dev-dev.com");
    Database db;
    //NotificationArea na;

    if (!db.isOpen())
    {
        qDebug() << QCoreApplication::tr("Failed to open the database. Shutdown.");
        return -1;
    }

    if (!db.createDatabaseTables())
    {
        qDebug() << QCoreApplication::tr("Failed to create database. Shutdown.");
        return -1;
    }

    Settings settings;
    settings.loadSettings();

    SpeechToText stt(settings.getAppActivation(),settings.getFastCommandsActivation()); // Объявление классов для его дальнейшего использования
    QQmlApplicationEngine engine; // Объявление класса для работы с qml
    QQmlContext* context = engine.rootContext();

    //qmlRegisterType<Database>("db", 1, 1, "db");
    //context->setContextProperty("na", &na);
    context->setContextProperty("db", &db);
    context->setContextProperty("stt", &stt); // Создание видимости qml-ем экземпляра класса SpeechToText


    context->setContextProperty("settings", (QObject*)&settings);


    stt.setGlobalShortcuts(db.receiveShortcuts());

    const QUrl url("qrc:/Main/Main.qml"); // Объявление url main-файла qml
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);


    app.setWindowIcon(QIcon(":/resources/img/trayicon.svg"));
    return app.exec();
}
