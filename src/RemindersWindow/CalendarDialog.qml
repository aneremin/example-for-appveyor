/**
  * @file
  * @brief Календарь
  *
  * Диалог, в котором можно выбрать дату
*/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

/*!
    \class CalendarDialog
    \brief Диалог, в котором можно выбрать дату
 */
Dialog {
    id: dialogCalendar
    // Задаём размеры диалогового окна
    width: 400
    height: 400

    /** @brief Сигнал о том, что нажата кнопка "Ok"*/
    signal okClicked
    /** @brief Выбранная дата */
    property date selectDate: calendar.selectedDate
    /** @brief Текущая дата */
    property var currentDate: new Date();

    // Создаем контент диалогового окна
    contentItem: Rectangle {
        id: dialogRect
        color: "#343842"

        // Первым идёт кастомный календарь
        Calendar {
            id: calendar

            // Размещаем его в верхней части диалога и растягиваем по ширине
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: row.top

            anchors.leftMargin: 3
            anchors.rightMargin: 3

            minimumDate: currentDate

            // Стилизуем Календарь
            style: CalendarStyle {

                // Стилизуем navigationBar
                navigationBar: Rectangle {
                    /* Он будет состоять из прямоугольника,
                     * в котором будет располагаться две кнопки и label
                     * */
                    height: 48
                    color: "#343842"

                    /* Горизонтальный разделитель,
                     * который отделяет navigationBar от поля с  числами
                     * */
                    Rectangle {
                        color: "#CE1A33"
                        height: 1
                        width: parent.width
                        anchors.bottom: parent.bottom
                    }

                    // Кнопка промотки месяцев назад
                    Button {
                        id: previousMonth
                        width: parent.height - 8
                        height: width
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 8

                        visible: currentDate.getMonth() === calendar.visibleMonth ? false : true

                        /* По клику по кнопке вызываем функцию
                         * календаря, которая отматывает месяц назад
                         * */
                        onClicked: control.showPreviousMonth()

                        // Стилизуем кнопку
                        style: ButtonStyle {
                            background: Rectangle {
                                // Окрашиваем фон кнопки
                                color: "#343842"
                                /* И помещаем изображение, у которго будет
                                 * два источника файлов в зависимости от того
                                 * нажата кнопка или нет
                                 */
                                Image {
                                    source: control.pressed ? "" : "/resources/img/left_arrow.png"
                                    width: parent.height - 8
                                    height: width
                                }
                            }
                        }
                    }

                    // Помещаем стилизованный label
                    Label {
                        id: dateText
                        /* Забираем данные из title календаря,
                         * который в данном случае не будет виден
                         * и будет заменён данным label
                         */
                        text: styleData.title
                        color:  "#fff"
                        elide: Text.ElideRight
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 16
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: previousMonth.right
                        anchors.leftMargin: 2
                        anchors.right: nextMonth.left
                        anchors.rightMargin: 2
                    }

                    // Кнопка промотки месяцев вперёд
                    Button {
                        id: nextMonth
                        width: parent.height - 8
                        height: width
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right

                        /* По клику по кнопке вызываем функцию
                         * календаря, которая отматывает месяц назад
                         * */
                        onClicked: control.showNextMonth()

                         // Стилизуем кнопку
                        style: ButtonStyle {
                            // Окрашиваем фон кнопки
                            background: Rectangle {
                                color: "#343842"
                                /* И помещаем изображение, у которго будет
                                 * два источника файлов в зависимости от того
                                 * нажата кнопка или нет
                                 */
                                Image {
                                    source: control.pressed ? "" : "/resources/img/right_arrow.png"
                                    width: parent.height - 8
                                    height: width
                                }
                            }
                        }
                    }
                }

                dayOfWeekDelegate: Rectangle {
                    color: "#6434AB"
                    implicitHeight: 30
                    Label {
                        text: control.locale.dayName(styleData.dayOfWeek, control.dayOfWeekFormat)
                        anchors.centerIn: parent
                        font.pixelSize: 16
                    }
                }

                // Стилизуем отображением квадратиков с числами месяца
                dayDelegate: Rectangle {
                    anchors.fill: parent
                    anchors.margins: styleData.selected ? -1 : 0
                    // Определяем цвет в зависимости от того, выбрана дата или нет
                    color: styleData.date !== undefined && styleData.selected ? selectedDateColor : "#7652AC"

                    // Задаём предопределённые переменные с цветами, доступные только для чтения
                    readonly property color sameMonthDateTextColor: "white"
                    readonly property color selectedDateColor: "#CE1A33"
                    readonly property color selectedDateTextColor: "white"
                    readonly property color differentMonthDateTextColor: "#979797"
                    readonly property color invalidDateColor: "#444"

                    // Помещаем Label для отображения числа
                    Label {
                        id: dayDelegateText
                        text: styleData.date.getDate() // Устанавливаем число в текущий квадрат
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignRight
                        font.pixelSize: 16

                        // Установка цвета
                        color: {
                            var theColor = invalidDateColor; // Устанавливаем невалидный цвет текста
                            if (styleData.valid) {
                                /* Определяем цвет текста в зависимости от того
                                 * относится ли дата к выбранному месяцу или нет
                                 * */
                                theColor = styleData.visibleMonth ? sameMonthDateTextColor : differentMonthDateTextColor;
                                if (styleData.selected)
                                    // Перекрашиваем цвет текста, если выбрана данная дата в календаре
                                    theColor = selectedDateTextColor;
                            }
                            theColor;
                        }
                    }
                }
            }
        }

        Row {
            id: row
            height: 50
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            anchors.bottomMargin: 2
            anchors.rightMargin: 2
            anchors.leftMargin: 2



            // Кнопка для закрытия диалога
            Button {
                id: dialogButtonCalCancel
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width / 2 - 1

                style: ButtonStyle {
                    background: Rectangle {
                        gradient: dialogButtonCalCancel.pressed ? style.getAttentionGradientEntered() : style.getAttentionGradientStandart()
                        border.width: 1
                    }


                    label: Text {
                        text: qsTr("Cancel") + settings.emptyString
                        font.pixelSize: 14
                        color: "#fff"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
               }

                // По нажатию на кнопку - просто закрываем диалог
                onClicked: dialogCalendar.close()
            }

            // Вертикальный разделитель между кнопками
            Rectangle {
                id: dividerVertical
                width: 2
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                color: "#343842"
            }

            // Кнопка подтверждения выбранной даты
            Button {
                id: dialogButtonCalOk
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width / 2 - 1

                style: ButtonStyle {
                    background: Rectangle {
                        gradient: dialogButtonCalOk.pressed ? style.getGradientEntered() : style.getGradientStandart()
                        border.width: 1
                    }


                    label: Text {
                        text: qsTr("Ok") + settings.emptyString
                        font.pixelSize: 14
                        color: "#fff"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }

                /* По клику по кнопке сохраняем выбранную дату во временную переменную
                 * и помещаем эту дату на кнопку в главном окне,
                 * после чего закрываем диалог
                 */
                 onClicked: {
                         dialogCalendar.okClicked()
                 }
            }
        }
     }

    /** @brief Данная функция необходима для того, чтобы
     * установить дату с кнопки в календарь,
     * иначе календарь откроется с текущей датой
     */
    function show(x){
        calendar.selectedDate = x
        dialogCalendar.open()
    }
}
