/**
  * @file
  * @brief Таблица напоминаний
  *
*/

import QtQuick 2.0
import QtQuick 2.14
import QtQuick.Controls 2.14

/*!
    \class ReminderTable
    \brief Таблица напоминаний
 */


GridView {
    id: reminder_table

    clip: true

    property string textColor

    header: Item {
        width: parent.width
        height: 10
    }

    /** @brief Функция получения ID  */
    function getID(){
        return rem_list.get(reminder_table.currentIndex).remid
    }

    function getValuesForEditing() {

        time_field.text = rem_list.get(reminder_table.currentIndex).timePlase
        date_field.text = rem_list.get(reminder_table.currentIndex).datePlase
        task_plase.text = rem_list.get(reminder_table.currentIndex).taskText
        details.text = rem_list.get(reminder_table.currentIndex).detailsText
        repeat_box.currentIndex = rem_list.get(reminder_table.currentIndex).repeatIndex
        countOfRepeat.text = rem_list.get(reminder_table.currentIndex).frequency
        time_field1.text = rem_list.get(reminder_table.currentIndex).period
        comandsList.setGradients(db.toList(rem_list.get(reminder_table.currentIndex).commands))
    }

    /** @brief Функция перезагрузки таблицы напоминаний */
    function reload_table(){

        /*var count = rem_list.count
        console.debug(rem_list.count)
        var j = 0
        for (; j < count;) {
            console.debug(j)
            rem_list.remove(0)
            j += 1
        }*/
        rem_list.clear()
        var list = db.selectAllTuples(9, table)

        var i = 1
        if (list[0] > 1)
        {
            for (; i < list[0];) {
                //console.log("equals: ",db.getDate(list[i + 2]),date_view.get_reminder_date())
                if (db.getDate(list[i + 2]) === date_view.get_reminder_date()){
                    rem_list.append({taskText: list[i + 1],
                                        datePlase: db.getDate(list[i + 2]),
                                        timePlase: db.getTime(list[i + 2]),
                                        detailsText: list[i + 3],
                                        repeatIndex: db.toInt(list[i + 4]),
                                        frequency: list[i + 5],
                                        period: list[i + 6],
                                        remid: list[i],
                                        isViewed : list[i + 7],
                                        commands : list[i + 8]})//scrcol.getCommands()})
                }
                i = i + 9
            }
        }
        //rem_table.model = rem_list
        //reminder_window.save_clicked = true

    }

    //function editReminder() {
    //    rem_list.setProperty(reminder_table.currentIndex,"taskText",task_plase.text)
    //    rem_list.setProperty(reminder_table.currentIndex,"datePlase",date_field.text)
    //    rem_list.setProperty(reminder_table.currentIndex,"timePlase",time_field.text)
    //    rem_list.setProperty(reminder_table.currentIndex,"detailsText",details.text)
    //    rem_list.setProperty(reminder_table.currentIndex,"repeatIndex",repeat_box.currentIndex)
    //    rem_list.setProperty(reminder_table.currentIndex,"frequency",db.toInt(countOfRepeat.text))
    //    rem_list.setProperty(reminder_table.currentIndex,"period",time_field1.text)
    //    rem_list.setProperty(reminder_table.currentIndex,"isViewed","0")
    //    //comandsList.setGradients(db.toList(rem_list.get(reminder_table.currentIndex).commands))
    //    //rem_list.setProperty(reminder_table.currentIndex,"commands", comandsList.commList)
    //    reload_table()
    //}

    ListModel {
        id: rem_list
        onCountChanged: {
            if (count > 0)
            {
                emptyText.visible = false
            } else {
                emptyText.visible = true
            }
        }
    }
    Component.onCompleted: {
        reload_table()
    }

    Text {
        id: emptyText
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        text: qsTr("Here is empty") + settings.emptyString
        color: textColor
        font.pixelSize: 15
        visible: false
        opacity: 0.6
    }

    //orientation: ListView.Horizontal

    //spacing: 50

    highlight: Rectangle {
        /*width: 1
        height: 1
        border.width: 2
        border.color: "#CE1A33"
        radius: 4*/
        color: "transparent"
        z: 2
        x: (reminder_table.currentItem) ? reminder_table.currentItem.x : 0
        y: (reminder_table.currentItem) ? reminder_table.currentItem.y : 0
        Behavior on x { SpringAnimation { spring: 3; damping: 0.2 } }
        Behavior on y { SpringAnimation { spring: 3; damping: 0.2 } }
        Text {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: -5

            text: qsTr("current") + settings.emptyString
            color: "#C12522"
        }
    }
    highlightFollowsCurrentItem: true

    /** @brief Добавление напоминания */
    function addingReminder()
    {
        rem_list.append({taskText: task_plase.text,
                            datePlase: date_field.text,
                            timePlase: time_field.text,
                            detailsText: details.text,
                            repeatIndex: repeat_box.currentIndex,
                            frequency: db.toInt(countOfRepeat.text),
                            period: time_field1.text,
                            remid: db.getId(),
                            isViewed : "0"})
        reload_table()
    }

    /** @brief Удаление напоминания */
    function removeReminder()
    {
        rem_list.remove(currentIndex)
    }

    MouseArea{
        anchors.fill: parent
        z: -1
        onWheel: {
            if (wheel.angleDelta.y > 0)
            {
                if (parent.currentIndex !== reminder_table.count - 1)
                    parent.currentIndex += 1
            }
            else
            {
                if (parent.currentIndex != 0)
                    parent.currentIndex -= 1
            }
        }
    }

    Rectangle {
        width: parent.width
        height: parent.height
        color: "black"
        opacity: 0.1
        radius: 5
        z: -1
    }

    model: rem_list

    cellHeight: 200
    cellWidth: (reminder_table.width)/3

    delegate: Item {

        id: dele

        /** @brief ID напоминания */
        //property string rem_id: remid
        height: cellHeight
        width: cellWidth

        /** @brief Функция возможности изменения элемента */
        property bool enable: false

        Rectangle {

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    currentIndex = model.index
                }
                onDoubleClicked: {
                    currentIndex = model.index
                    add_button.isAdd = false
                    getValuesForEditing()
                    addReminder.open()
                }
            }
            opacity: (isViewed == "1") ? 0.5 : 1
            height: parent.height - 20
            width: parent.width - 20
            anchors.left: parent.left
            anchors.leftMargin: 10
            radius: 10
            gradient:  Gradient {

                GradientStop {
                    position: 0
                    color: "#fdfbfb"
                }
                GradientStop {
                    position: 1
                    color: "#ebedee"
                }
            }
            //color: "transparent"
            Row {
                id: rowDate
                topPadding: 10
                spacing: 10//parent.width - taskText1.width - timePlase1.width - 20
                width: parent.width - spacing - leftPadding*2
                leftPadding: 5
                Rectangle {


                    width: parent.width - timePlase1.width - 5
                    height: 30 - border.width
                    radius: 5
                    color: "transparent"

                    Rectangle {
                        //anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        width: taskText1.width
                        height: 1
                        color: "#CE1A33"
                    }
                    Rectangle {
                        //anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.bottom
                        width: taskText1.width
                        height: 1
                        color: "#CE1A33"
                    }
                    Text {
                        id: taskText1
                        text: taskText//taskText.substr(0,15) + ((taskText.length > 15) ? "..." : "" )
                        leftPadding: 10
                        //topPadding: 10
                        color: "black"
                        width: parent.width
                        font.pixelSize: 20
                        clip: true
                    }
                }

                Rectangle {
                    gradient: style.getGradientStandart()
                    width: 60
                    height: 30
                    radius: 5
                    Text {
                        id: timePlase1
                        text: timePlase
                        anchors.fill: parent
                        anchors.margins: 2
                        horizontalAlignment: Text.AlignHCenter
                        //topPadding: 10
                        color: "white"
                        font.pixelSize: 20
                    }
                }
            }

            Text {
                id: detailsEdit
                objectName:"detailsEdit"
                text: detailsText
                width: parent.width
                height: 80
                anchors.top: rowDate.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10

                color: "black"//style.getTextColor()
                font.pixelSize: 20
                wrapMode: TextInput.WrapAnywhere

                clip: true

            }
            /* Flickable {
                id: flickableDetailsText
                objectName: "flickableDetailsText"
                width: parent.width
                height: 100
                anchors.top: rowDate.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                flickableDirection: Flickable.VerticalFlick
                TextArea.flickable: TextArea {
                    id: detailsEdit
                    objectName:"detailsEdit"
                    text: detailsText
                    readOnly: true
                    //rightInset: 10
                    background: Rectangle{
                        color: "transparent"
                        border.color: "gray"
                        border.width: 1
                        opacity: 0.5
                    }

                    selectByMouse: true
                    placeholderText: qsTr("Enter details") + settings.emptyString
                    placeholderTextColor: "#fffddd"//style.getPlaceholderTextColor()
                    color: "black"//style.getTextColor()
                    font.pixelSize: 20
                    wrapMode: TextInput.WrapAnywhere
                }
                clip: false

                ScrollBar.vertical: ScrollBar {}
                ScrollBar.horizontal: null
            }*/

            Rectangle {
                id: ticker
                anchors.top: detailsEdit.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                height: 20
                //width: 450
                //color: "blue"

               // gradient: style.getGradientStandart()
                color: "transparent"

                clip: true

                onWidthChanged: {
                    console.log("WIDTH CHAGE!!")
                    console.log(tickerRow.tickerWidth)
                    //tickerStart.stop()
                    if (tickerRow.tickerWidth != 0){
                        tickerStart.stop()
                        tickerRow.tickerWidth = ticker.width
                        tickerStart.start()
                        console.log(tickerRow.tickerWidth)
                    }
                }

                Row{
                    spacing: 10
                    clip: true
                    id: tickerRow

                    property var widthOfCommands
                    property var tickerWidth: ticker.width
                    //property var durationValue

                    NumberAnimation on x { id: tickerStart; running: false; from: tickerRow.tickerWidth; to: -tickerRow.widthOfCommands  ; duration: 10000; loops: Animation.Infinite }
                    Repeater{
                        id: repeater
                        model: db.toList(commands).length
                        clip: true

                        delegate: Rectangle {
                            property var command: comandName.text
                            property var vidth: width

                            gradient: style.getGradientStandart()
                            radius: 5


                            height: 20
                            clip: true
                            width: comandName.width
                            Text {
                                id: comandName
                                color: "white"
                                text: parent.command
                            }
                        }

                        Component.onCompleted: {
                            var com = db.toList(commands)
                            var width = 0
                            for (var i = 0; i < repeater.count; ++i){
                                repeater.itemAt(i).command = com[i]
                                width += repeater.itemAt(i).width + tickerRow.spacing
                            }
                            tickerRow.widthOfCommands = width
                            //tickerRow.durationValue = repeater.model * 5000

                            console.log(width + "!!!")

                            tickerStart.start()
                        }
                    }
                }
            }
        }
    }

    add: Transition {
        NumberAnimation { properties: "x,y"; easing.amplitude: 40;easing.type: Easing.InOutQuad; duration: 500 }
        //SpringAnimation { spring: 3; damping: 0.2 }
    }
    displaced: Transition {
        NumberAnimation { properties: "x,y"; easing.type: Easing.InOutQuad;duration: 500 }
    }

    remove: Transition {
        ParallelAnimation {
            NumberAnimation { property: "opacity"; to: 0; duration: 1000 }
            NumberAnimation { properties: "x,y"; to: 100; duration: 1000 }
        }
    }
    ScrollBar.horizontal:  ScrollBar {
        id: scroll_bar
        anchors.top:  reminder_table.bottom
        height: 10
        active: true

        contentItem: Rectangle {
            implicitHeight:4
            radius: implicitHeight/2
            color: "Gray"
            opacity:  0.5
        }
    }
}
