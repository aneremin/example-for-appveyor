/**
  * @file
  * @brief Грид с отображения возможных дат напоминаний
  *
*/

import QtQuick 2.0
import QtQuick 2.14
import QtQuick.Controls 2.14

/*!
    \class ReminderDateTable
    \brief  Грид с отображения возможных дат напоминаний
 */
Rectangle{
    id: date_view

    //height: 30
    signal dateTimeChanged
    clip: true
    property string textColor
    property string locale: (settings.getActiveLanguage() === "en" ? "en_US" : "ru_RU")

    Component.onCompleted: {
        console.log(">>>",settings.getActiveLanguage(), locale)
    }

    //signal currentIndexChanged
    /** @brief Получение выбранной даты */
    function get_reminder_date(){
        return new Date(currentYear,
                        currentMonth,
                        currentDay).toLocaleDateString(Qt.locale(locale),"yyyy.MM.dd")
    }

    function updLanguage() {

        locale = settings.getActiveLanguage() === "en" ? "en_US" : "ru_RU"
        for (var i = 0; i < 12; i++)
        {
            monthsModel.get(i).value = new Date(currentYear,
                                                i,
                                                1).toLocaleDateString(Qt.locale(locale),"MMM")
        }

        currentDateTimeText.update()
    }

    property var prevX

    /** @brief массив дат */
    property var dateArray: []

    property var currentDay: new Date().getDate();
    property var currentMonth: new Date().getMonth();
    property var currentYear: new Date().getFullYear();
    ListModel {
        id: daysModel
        Component.onCompleted:{
            for (var i = 1; i < 32; i++)
            {
                append({value: i})
            }
        }
    }
    ListModel {
        id: monthsModel
        Component.onCompleted:{
            for (var i = 0; i < 12; i++)
            {
                append({value: new Date(currentYear,
                                        i,
                                        1).toLocaleDateString(Qt.locale(locale),"MMM"), num: i})
            }
        }
    }
    ListModel {
        id: yearsModel
        Component.onCompleted:{
            var lastYear = new Date().getFullYear() + 30
            for (var i = 1970; i < lastYear; i++)
            {
                append({value: i})
            }
        }
    }

    Rectangle {
        //rotation: 90
        anchors.fill: parent
        anchors.bottom: undefined
        anchors.topMargin: 110
        color: "transparent"
        anchors.horizontalCenter: parent.horizontalCenter
        Text {
            id: currentDateTimeText
            anchors.fill: parent
            color: textColor
            horizontalAlignment: Text.AlignHCenter
            text: new Date(currentYear,
                           currentMonth,
                           currentDay).toLocaleDateString(Qt.locale(locale))
        }
    }

    property var prevDay : new Date().getDate() - 1;
    //property var prevMonth : new Date().getMonth();
    //property var prevYear : new Date().getFullYear();
    //property var startIndex: new Date().getDate() - 1
    property bool isWheel: false

    Tumbler {
        id: hours_tumbler
        z:2
        //currentIndex: startIndex
        //spacing: 20
        rotation: -90
        anchors.horizontalCenter: parent.horizontalCenter
        //anchors.fill: parent
        //width: parent.height
        //height: parent.width
        //anchors.top: parent.parent.parent.top
        height: 300
        width: 70
        state: "day"

        states: [
            State {
                name: "day"
                PropertyChanges {
                    target: hours_tumbler
                    model: daysModel
                }
            }, State {
                name: "month"
                PropertyChanges {
                    target: hours_tumbler
                    model: monthsModel
                }
            },State {
                name: "year"
                PropertyChanges {
                    target: hours_tumbler
                    model: yearsModel
                }
            }
        ]


        //width: parent.width / 3
        model: daysModel

        visibleItemCount: 7

        function getMonth(month,back) {
            if (back) {
                if (month === 0)
                {
                    currentYear--
                    return 11
                }
                else
                    return month - 1
            } else {
                if (month === 11)
                {
                    currentYear++
                    return 0
                }
                else
                    return month + 1
            }
        }

        onCountChanged: {

            if (state === "day")
                currentIndex = currentDay - 1
            else if (state === "year")
                currentIndex = currentYear - 1970
            else if (state === "month")
            {
                console.log(">>>>",currentMonth)
                currentIndex = currentMonth
            }
        }
        onStateChanged: {
            isWheel = true
        }

        onCurrentIndexChanged: {

            if (isWheel)
            {

                isWheel = false
                return
            }


            console.log(currentIndex,prevDay)

            if (hours_tumbler.state === "day") {
                if (currentIndex === prevDay + 1)
                {
                    var lastDate = new Date(currentYear,currentMonth + 1,0).getDate() - 1
                    if (currentIndex > lastDate)
                    {
                        console.log(">>>",currentIndex,lastDate)
                        currentMonth = getMonth(currentMonth,false)
                        prevDay = 0
                        currentIndex = 0
                        currentDay = 1
                    }
                    else
                    {
                        prevDay = currentIndex
                        currentDay = currentIndex + 1
                    }
                } else if (currentIndex + 15 < prevDay) {
                    currentMonth = getMonth(currentMonth,false)
                    prevDay = 0
                    currentIndex = 0
                    currentDay = 1
                }
                else if (currentIndex === prevDay - 1)
                {
                    prevDay = currentIndex
                    currentDay = currentIndex + 1
                    //currentMonth = getMonth(currentMonth,true)

                } else if (currentIndex > prevDay + 15) {
                    currentMonth = getMonth(currentMonth,true)
                    lastDate = new Date(currentYear,currentMonth + 1,0).getDate() - 1
                    prevDay = lastDate
                    currentIndex = lastDate
                    currentDay = lastDate + 1
                }

            } else if (hours_tumbler.state === "month") {
                currentMonth = monthsModel.get(hours_tumbler.currentIndex).num
            } else if (hours_tumbler.state === "year") {
                currentYear = yearsModel.get(hours_tumbler.currentIndex).value
            }
            dateTimeChanged()

        }

        MouseArea{

            anchors.fill: parent

            onWheel: {
                isWheel = true
                if (hours_tumbler.state === "day") {


                    if (wheel.angleDelta.y > 0)
                    {
                        var lastDate = new Date(currentYear,currentMonth + 1,0).getDate() - 1
                        if ( parent.currentIndex === lastDate)
                        {
                            currentMonth = parent.getMonth(currentMonth,false)
                            parent.currentIndex = 0
                            currentDay = 1
                        } else {

                            parent.currentIndex += 1
                            currentDay++
                        }
                    }
                    else
                    {

                        lastDate = new Date(currentYear,currentMonth,0).getDate() - 1
                        if (parent.currentIndex === 0)
                        {
                            currentMonth = parent.getMonth(currentMonth,true)
                            parent.currentIndex = lastDate
                            currentDay = lastDate + 1
                        } else {

                            parent.currentIndex -= 1
                            currentDay--
                        }
                    }
                    prevDay = parent.currentIndex
                } else if (hours_tumbler.state === "month") {
                    var count = hours_tumbler.count - 1
                    if (wheel.angleDelta.y > 0)
                    {
                        hours_tumbler.currentIndex = (hours_tumbler.currentIndex + 1 > count) ? 0
                                                                                           : hours_tumbler.currentIndex + 1
                        currentMonth = hours_tumbler.currentIndex

                    } else {
                        hours_tumbler.currentIndex = (hours_tumbler.currentIndex - 1 < 0) ? count
                                                                                           : hours_tumbler.currentIndex - 1
                        currentMonth = hours_tumbler.currentIndex
                    }


                } else if (hours_tumbler.state === "year") {
                    count = hours_tumbler.count - 1
                    if (wheel.angleDelta.y > 0)
                    {
                        hours_tumbler.currentIndex = (hours_tumbler.currentIndex + 1 > count) ? 0
                                                                                           : hours_tumbler.currentIndex + 1
                        currentYear = yearsModel.get(hours_tumbler.currentIndex).value

                    } else {
                        hours_tumbler.currentIndex = (hours_tumbler.currentIndex - 1 < 0) ? count
                                                                                           : hours_tumbler.currentIndex - 1
                        currentYear = yearsModel.get(hours_tumbler.currentIndex).value
                    }
                }
                dateTimeChanged()
            }

            /*         onPressed: {
                prevX = mouseX
            }

            onPressAndHold: {
                console.log(prevX,mouseX)
                if (prevX < mouseX)
                {
                    var lastDate = new Date(currentYear,currentMonth + 1,0).getDate() - 1
                    if ( parent.currentIndex >= lastDate)
                    {
                        currentMonth = parent.getMonth(currentMonth,false)
                        parent.currentIndex = 0
                    } else {

                        parent.currentIndex += 1
                    }
                }
                else if (prevX > mouseX)
                {

                    lastDate = new Date(currentYear,currentMonth,0).getDate() - 1
                    if (parent.currentIndex >= 0)
                    {
                        currentMonth = parent.getMonth(currentMonth,true)
                        parent.currentIndex = lastDate
                    } else {

                        parent.currentIndex -= 1
                    }
                }
            }
*/
        }

        background: Item {
            rotation: 90
            /*Rectangle {
                opacity: hours_tumbler.enabled ? 0.2 : 0.1
                border.color: "#6136A3"
                width: parent.width
                height: 1
                anchors.top: parent.top
            }

            Rectangle {
                opacity: hours_tumbler.enabled ? 0.2 : 0.1
                border.color: "#6136A3"
                width: parent.width
                height: 1
                anchors.bottom: parent.bottom
            }*/
        }


        delegate: Text {
            rotation: 90
            id: hours
            text: qsTr(value < 10 ? "0" + "%0" : "%0").arg(value)
            color: textColor
            font: hours_tumbler.font
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            opacity: 1.0 - Math.abs(Tumbler.displacement) / (hours_tumbler.visibleItemCount / 2)
            MouseArea {
                anchors.fill: parent
                onPressAndHold: {

                    if (hours_tumbler.state === "day")
                        hours_tumbler.state = "month"
                    else if (hours_tumbler.state === "month")
                        hours_tumbler.state = "year"
                }
                onClicked: {
                    if (hours_tumbler.state === "year")
                        hours_tumbler.state = "month"
                    else if (hours_tumbler.state === "month")
                        hours_tumbler.state = "day"
                }
            }
        }

        Rectangle {
            anchors.horizontalCenter: hours_tumbler.horizontalCenter
            y: hours_tumbler.height * 0.4
            width: 40
            height: 1
            color: "#CE1A33"
        }

        Rectangle {
            anchors.horizontalCenter: hours_tumbler.horizontalCenter
            y: hours_tumbler.height * 0.6
            width: 40
            height: 1
            color: "#CE1A33"
        }
    }

}
