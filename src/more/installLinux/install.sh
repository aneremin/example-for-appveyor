#!/bin/bash

iconsPath="/usr/share/icons/hicolor/scalable/apps"
desktopPath="/usr/share/applications" 
installPath="/opt/deskhelper"
linkPath="/usr/bin/deskhelper"

#Check user
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root"
    exit 1
fi

#Remove old version
if [ -d $installPath ]; then
    $installPath'/uninstall.sh' > /dev/null   
fi
    
echo "deskhelper install..."
mkdir -v $installPath
cp -v -r bin $installPath
cp -v -r lib $installPath
cp -v -r plugins $installPath
cp -v -r qml $installPath
cp -v -r translations $installPath
cp -v deskhelper.sh $installPath

cp -v uninstall.sh $installPath
cp -v deskhelper.desktop $desktopPath
ln -sv $installPath'/deskhelper' $linkPath
chmod a+x $linkPath
echo "---"
cp -v icons/deskhelper.svg $iconsPath
gtk-update-icon-cache /usr/share/icons/hicolor
update-desktop-database

echo "...finished!"