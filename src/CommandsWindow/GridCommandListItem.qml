/**
  * @file
  * @brief Полоска текста с закругленным фоном
  *
*/

import QtQuick 2.0


Rectangle
{
    /** @brief Текст полоски*/
    property string itemName
    /** @brief Цвет границ полоски*/
    property string border_color//: style.getBorderColor()
    /** @brief Цвет текста*/
    property string text_color
    /** @brief Градиент полоски*/
    property Gradient backgroundGrad
    /** @brief Степень закругленности полоски*/
    property int _radius: 4
    color: "transparent"

    Rectangle
    {
        gradient: backgroundGrad
        anchors.fill: parent
        border.width: 0
        border.color: border_color
        radius: _radius
        opacity: 0.1
    }

    Text {
        anchors.fill:parent

        anchors.margins: 5
        width:parent.width
        height: parent.height

        text: itemName
        wrapMode: Text.WrapAnywhere
        font.pointSize: 12
        color: text_color
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}

