#ifndef SETTINGS_H
#define SETTINGS_H


#include <QtGui>
#include <QCoreApplication>
/*!
    \class Settings
    \brief Класс для хранения, загрузки и сохранения настроек
*/
class Settings : public QObject
{
 Q_OBJECT
 Q_PROPERTY(QString emptyString READ getEmptyString NOTIFY languageChanged)

 public:
  Settings();

  QString getEmptyString();

 Q_INVOKABLE void selectLanguage(QString language);
public slots:
  bool loadSettings();
  bool generateSettings();
  bool saveSettings(bool BsilentMode,
                    bool btrayMode,
                    bool bdarkMode,
                    QString shortcut,
                    QString language,
                    bool bpromptMode,
                    QString fastCommandsShortcut);
  void setStartWindowViewed(bool viewing);
  void setSilentMode(bool mode);
  void setTrayMode(bool mode);
  void setDarkMode(bool mode);
  void setPromptMode(bool mode);
  void setActiveLanguage(QString language);
  void setAppActivation(QString shortcut);
  void setFastCommandsActivation(QString shortcut);

  bool getStartWindowViewed();
  bool getSilentMode();
  bool getTrayMode();
  bool getDarkMode();
  bool getPromptMode();
  QString getActiveLanguage();
  QString getAppActivation();
  QString getFastCommandsActivation();
  QString getErrorText();
 signals:
  void languageChanged();
  void themeChanged();
  void trayModeChanged();
  void promptModeChanged();

 private:
  /*! \brief Указатель на экземпляр QTranslator */
  QTranslator *translator1;
  /*! \brief Параметр, означающий, открыло ли окно приветствия - по умолчанию - закрыто*/
  bool startWindowViewed = false;
  /*! \brief Параметр, означающий, включен ли режим "без звука" - по умолчанию - выключен */
  bool silentMode = false;
  /*! \brief Параметр, означающий, включен ли режим сворачивания в трей - по умолчанию - выключен */
  bool trayMode = false;
  /*! \brief Параметр, означающий, включена ли темная тема - по умолчанию - включена */
  bool darkMode = true;
  /*! \brief Параметр, означающий, включены ли подсказки - по умолчанию - выключены */
  bool promptMode = false;
  /*! \brief Параметр, язык приложения - по умолчанию - английский */
  QString activeLanguage = "en";
  /*! \brief Параметр, сочетание клавиш для активации приложения - по умолчанию - Ctrl+Alt+H */
  QString appActivation = "Ctrl+Alt+H";
  /*! \brief Параметр, сочетание клавиш для активации окна быстрого доступа к командам - по умолчанию - Ctrl+Alt+F */
  QString fastCommandsActivation = "Ctrl+Alt+F";
  /*! \brief Текст ошибки */
  QString errorText;
};


#endif // SETTINGS_H
