#ifndef ACTION_H
#define ACTION_H

#include <QObject>
#include <QProcess>
#include <QDebug>

class Action : public QObject
{
    Q_OBJECT
public:
    Action();
    void setAction(QString a);
public slots:
    void doAction();
signals:
    void workFinished();
private:
    QProcess* process;
    QString action;
};

#endif // ACTION_H
