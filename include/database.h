#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QJsonObject>
#include <QJsonDocument>
#include <QKeySequence>
#include <QQuickTextDocument>
#include "../include/levenshtein.h"
#include <QDate>


#define COUNT_OF_PARAMS_IMAGESLIST 3

/*!
    \class Database
    \brief Класс для связи приложения и БД
 */

class Database : public QObject
{
    Q_OBJECT
public:
    Database();
    ~Database();

public slots:

    bool isOpen();
    bool createDatabaseTables();
    bool addTuple(QVariantList v, QString table);
    QVariantList selectAllTuples(int countOfParam, QString table);
    bool makeImageMain(QString tag);
    bool removeTuple(QJsonObject key, QString table);
    bool editTuple(QJsonObject v, QString table, QJsonObject condition);
    QVariantList selectWithCond(QJsonObject condition,int countOfParam, QString table);
    QStringList receiveShortcuts();
    QString getImageMainPath();
    QStringList findCommand(QJsonObject json, QString column);
    QStringList findCommandWithLevenshteinDistance(QString string);
    QString getError();


    int getCountRows(QString table);


    QVariantList getReminderMessage(QString date, QString time);
    QString incrementReminderDate(int repetition, QString strDate);
    QVariantList getNotViewedNotifications (QString date);

    QDate stringToDate(QString strDate);
    QString getDate(QString dbDate);
    QString getTime(QString dbDate);
    QString getHours(QString dbDate);
    QString getMinutes(QString dbDate);


    QString getSymbol(int sym);
    QStringList getExpansion();
    QString trimString(QString str);
    QStringList toList(QString str, QString delimiter = ",");
    bool haveIntersection(QStringList list1, QStringList list2);
    int toInt(QString str);
    QString toLower(QString str);
    QString getId();
    QString changeAllQuotes(QString string, bool onSingle);
    QString changeFontFamily(QString string, QString newFontFamily);
    QString changeBold(QString formattedText, QString text);
    QString changeItalic(QString formattedText, QString text);
    QString changeUnderline(QString formattedText, QString text);
    QString getFontFamily(QString string);

private:
    /*! \brief Экземпляр QSqlDatabase для работы с SQLite */
    QSqlDatabase db;
    /*! \brief Строка с ошибкой для вывода на экран */
    QString errorStr;
    /*! \brief Символы латиницы на клавиатуре */
    const QString keyboardEn = "QWERTYUIOP[]ASDFGHJKL;'ZXCVBNM,.";
    /*! \brief Символы кириллицы на клавиатуре */
    const QString keyboardRu = "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ";
    /*! \brief id - для получения последнего добавленного элемента в таблицу БД */
    QString id;

};

#endif // DATABASE_H
