var searchData=
[
  ['placeholdertextcolor_1092',['placeholderTextColor',['../classTheme.html#ab7a2b8912e8fdeb662a6027c4bebf4d9',1,'Theme']]],
  ['prevday_1093',['prevDay',['../classReminderDateTable.html#a0ffffec4922ecb9f172a6098198af147',1,'ReminderDateTable']]],
  ['previousx_1094',['previousX',['../classCustomSystemPanel.html#ac6ab46cf03c51a4c24fa5d88f9445a68',1,'CustomSystemPanel::previousX()'],['../classResizeArea.html#a8ca0903f218c2f34c00b6bc3eedb2821',1,'ResizeArea::previousX()'],['../classMain.html#a23c4420e27caa2aa0652bbac6b73f701',1,'Main::previousX()']]],
  ['previousy_1095',['previousY',['../classCustomSystemPanel.html#a666db324353044cfcd4b74fafde9aabb',1,'CustomSystemPanel::previousY()'],['../classResizeArea.html#ac5e2c390095efba26d787c5d7ceb6931',1,'ResizeArea::previousY()'],['../classMain.html#a96ac1ad1d3fb92766227f6d97675bd5a',1,'Main::previousY()']]],
  ['prevx_1096',['prevX',['../classNotification.html#ac55d787e17e319b963923001718f730f',1,'Notification::prevX()'],['../classReminderDateTable.html#a8aeddaffd25699ed4ac78d0d9b141f1e',1,'ReminderDateTable::prevX()'],['../classRemindersDialog.html#af37baf5d5f768b3fa211c9051d66260e',1,'RemindersDialog::prevX()'],['../classStartWindow.html#a976bc593870290ac46982982c4892b44',1,'StartWindow::prevX()']]],
  ['prevy_1097',['prevY',['../classNotification.html#af54ac888313b488be21b167767f03381',1,'Notification::prevY()'],['../classRemindersDialog.html#ab77faee2e63cc7434191b5c6a49bf072',1,'RemindersDialog::prevY()'],['../classStartWindow.html#a08d74c4e8038a32dade9b1f24656d6ee',1,'StartWindow::prevY()']]],
  ['promptrotation_1098',['promptRotation',['../classGuideRectangle.html#a01d2f685fee76b3783d3afde99a34b06',1,'GuideRectangle']]],
  ['prompttext_1099',['promptText',['../classGuideRectangle.html#afc0bae34e442d0ff1a0e8c85e205ccf7',1,'GuideRectangle']]],
  ['proportion_1100',['proportion',['../classMain.html#afa6227246f8b374acfeb553caf692644',1,'Main']]]
];
