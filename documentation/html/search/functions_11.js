var searchData=
[
  ['savesettings_882',['saveSettings',['../classSettings.html#aafa870c4d48baefcbef254bff86bb8f0',1,'Settings']]],
  ['secondclick_883',['secondClick',['../classSpeechToText.html#a6e1f04d25cb6f41d641fe33a27677fbf',1,'SpeechToText']]],
  ['selectalltuples_884',['selectAllTuples',['../classDatabase.html#a952727945057b5316af22e3265e3f365',1,'Database']]],
  ['selectcheckedcats_885',['selectCheckedCats',['../classCreateCategory.html#a6126d42dc5c4ce6f37663e13a0d31e7f',1,'CreateCategory::selectCheckedCats()'],['../classNotes.html#a92bec100cf0dc2e1160a974708ffe5b8',1,'Notes::selectCheckedCats()']]],
  ['selectlanguage_886',['selectLanguage',['../classSettings.html#a78862e4cb646b76d5bdb9f4252697161',1,'Settings']]],
  ['selectwithcond_887',['selectWithCond',['../classDatabase.html#a45ec53b6f0fceca747dbd24f19a19115',1,'Database']]],
  ['sendtexttomainclass_888',['sendTexttoMainClass',['../classSpeechToText.html#a5055d9f2e676cde9ea05af004ad1a8d8',1,'SpeechToText']]],
  ['setaction_889',['setAction',['../classAction.html#a005ee476d22826cb38903d3af2e78d64',1,'Action']]],
  ['setactivelanguage_890',['setActiveLanguage',['../classSettings.html#ab2b28d7e0f01ccb848f26e6ae74095d8',1,'Settings']]],
  ['setappactivation_891',['setAppActivation',['../classSettings.html#a0c4da641b07ca5f093c58594a90e89e3',1,'Settings']]],
  ['setcomwinvisible_892',['setComWinVisible',['../classGeneralWindow.html#a338375e13e73696886a9598907dfa03b',1,'GeneralWindow']]],
  ['setdarkmode_893',['setDarkMode',['../classSettings.html#a5007c1f43dec283e73582bade728ae29',1,'Settings']]],
  ['setdisabled_894',['setDisabled',['../classQxtGlobalShortcut.html#afd5563a5b2b9b63b18ed7e3e7b67e048',1,'QxtGlobalShortcut']]],
  ['setenabled_895',['setEnabled',['../classQxtGlobalShortcut.html#a470a6799146d1693a23140ca55edbea6',1,'QxtGlobalShortcut']]],
  ['seterrortext_896',['setErrorText',['../classSpeechToText.html#a6448ad48037ed29b1444f3f2ba6308a1',1,'SpeechToText']]],
  ['setfastcommandsactivation_897',['setFastCommandsActivation',['../classSettings.html#adcf393bf6e9de00704c08f54f744b3e9',1,'Settings']]],
  ['setglobalshortcuts_898',['setGlobalShortcuts',['../classSpeechToText.html#af2028342adc6bb316ddb74efc2e28913',1,'SpeechToText']]],
  ['setgrad_899',['setGrad',['../classCharactersTable.html#a1c2b6b21623d5c3e491dfeb8ffae05b0',1,'CharactersTable']]],
  ['setimgvisible_900',['setImgVisible',['../classGeneralWindow.html#a422c53d87cc068f39cc1c24a8fb4f6b8',1,'GeneralWindow']]],
  ['setnotwinvisible_901',['setNotWinVisible',['../classGeneralWindow.html#a5fb6503698b63a13d23b7d2e1b645fb0',1,'GeneralWindow']]],
  ['setpromptmode_902',['setPromptMode',['../classSettings.html#a927ce8478b62ffc6b8edba40d85443fb',1,'Settings']]],
  ['setpromptmodeinsettings_903',['setPromptModeInSettings',['../classGeneralWindow.html#a3691d0cd8de3aaa44bdc6c7c0a86dbd0',1,'GeneralWindow']]],
  ['setpublic_904',['setPublic',['../classQxtPrivateInterface.html#a0aff7846be88207523a4f1993057c93b',1,'QxtPrivateInterface']]],
  ['setremwinvisible_905',['setRemWinVisible',['../classGeneralWindow.html#a791fb2f815426b0203c52320bc3eca78',1,'GeneralWindow']]],
  ['setrunning_906',['setRunning',['../classSpeechToText.html#a1a7d44b123206bc930d42a7728182c5d',1,'SpeechToText']]],
  ['setshortcut_907',['setShortcut',['../classQxtGlobalShortcut.html#aca42c3d5bc5a20cdca315cd398040cea',1,'QxtGlobalShortcut::setShortcut()'],['../classQxtGlobalShortcutPrivate.html#a896525de4bbde4deee06c0aeed201579',1,'QxtGlobalShortcutPrivate::setShortcut()']]],
  ['setshortcutappactivation_908',['setShortcutAppActivation',['../classSpeechToText.html#a558c2eeb8807794c1e5d0d5625081568',1,'SpeechToText']]],
  ['setshortcutfastcommandsactivation_909',['setShortcutFastCommandsActivation',['../classSpeechToText.html#ad76b3cebd06c67a2df4ea3b6ff1dbf9b',1,'SpeechToText']]],
  ['setsilentmode_910',['setSilentMode',['../classSettings.html#afe7ca0e2fe2bc2d05e768f75cd0dec12',1,'Settings']]],
  ['setstartwindowviewed_911',['setStartWindowViewed',['../classSettings.html#aab9609d2e476883d0bec2b61d6bf2e3f',1,'Settings']]],
  ['settings_912',['Settings',['../classSettings.html#ab7169a6eefce79566dd07db3b1e5e967',1,'Settings']]],
  ['settingschanged_913',['settingsChanged',['../classGeneralWindow.html#ade3dbe382a833194e19bba22fd9cdd96',1,'GeneralWindow']]],
  ['settraymode_914',['setTrayMode',['../classSettings.html#a4fe5d042afbbaf165a803b92ac7f1251',1,'Settings']]],
  ['setup_915',['Setup',['../classSetup.html#a0921e54e5a0200af117192e4c0c845b7',1,'Setup']]],
  ['shortcut_916',['shortcut',['../classQxtGlobalShortcut.html#ae3df6adc3d232ae00291c4e4f577f3ae',1,'QxtGlobalShortcut']]],
  ['show_917',['show',['../classCalendarDialog.html#afeec3a08810964a222ed53aa1e7ab501',1,'CalendarDialog::show()'],['../classCalendarLineDialog.html#a680c261bf59c67fa6ffd40b5c5409e9c',1,'CalendarLineDialog::show()']]],
  ['showapp_918',['showApp',['../classSpeechToText.html#a6f1e0eeb5ffc8c69c3812731101f899b',1,'SpeechToText']]],
  ['showcommandslist_919',['showCommandsList',['../classFastCommandsWindow.html#aef3e8b82ec78682565abfde7f9b12078',1,'FastCommandsWindow']]],
  ['showcommandwindow_920',['showCommandWindow',['../classMain.html#a36afb40e3d8b6fe9d72508c8935e0272',1,'Main']]],
  ['showelements_921',['showElements',['../classGuideRectangle.html#a88803ea83398659e43bf489d9b6a3b86',1,'GuideRectangle']]],
  ['showfastcommands_922',['showFastCommands',['../classSpeechToText.html#a93044b4979aaa64df15ce420407d615e',1,'SpeechToText']]],
  ['showfastcommandswindow_923',['showFastCommandsWindow',['../classMain.html#ac15750cfdb45e58b077730930c582888',1,'Main']]],
  ['showimageselectwindow_924',['showImageSelectWindow',['../classMain.html#aa9f4a81ff3d29108def3492e217d55b7',1,'Main']]],
  ['shownodeswindow_925',['showNodesWindow',['../classMain.html#a180a0ffc30f2c5d60c1f8f54a23039d9',1,'Main']]],
  ['showpromptchanged_926',['showPromptChanged',['../classGeneralWindow.html#afc57acc0854486cd40e337090445a8e9',1,'GeneralWindow']]],
  ['showprompts_927',['showPrompts',['../classImageSelectionMenu.html#a56902240de2603d0dd2bdca71989a934',1,'ImageSelectionMenu']]],
  ['showreminderwindow_928',['showReminderWindow',['../classMain.html#afff5e367bede5097b71b93f5b8ea5acf',1,'Main']]],
  ['showsettingswindow_929',['showSettingsWindow',['../classMain.html#a74993bd4023f5c1dc4b290959445e5a3',1,'Main']]],
  ['showwindow_930',['showWindow',['../classMain.html#a30b195295eec3e2503a626751d50f460',1,'Main']]],
  ['speechtotext_931',['SpeechToText',['../classSpeechToText.html#afff05baac3a3e19b7bdf387c90fcd977',1,'SpeechToText']]],
  ['stringtodate_932',['stringToDate',['../classDatabase.html#a9015d404f00b8b22df4f10547be3905e',1,'Database']]]
];
