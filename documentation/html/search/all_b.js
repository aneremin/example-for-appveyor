var searchData=
[
  ['languagechanged_265',['languageChanged',['../classSettings.html#a130fcb4d32379104628686cc96f127c1',1,'Settings']]],
  ['left_5farea_5fresize_266',['left_area_resize',['../classResizeArea.html#ae2393f05162be0d3db830ff8fcfaf25a',1,'ResizeArea']]],
  ['levenshtein_267',['levenshtein',['../levenshtein_8h.html#a54dfc3d113fd974bff89634280d45172',1,'levenshtein(QString string1, QString string2, int swap_penalty, int substition_penalty, int insertion_penalty, int deletion_penalty):&#160;levenshtein.cpp'],['../levenshtein_8cpp.html#a31e9065c8a8738ec1515a4631de9eace',1,'levenshtein(QString string1, QString string2, int w, int s, int a, int d):&#160;levenshtein.cpp']]],
  ['levenshtein_2ecpp_268',['levenshtein.cpp',['../levenshtein_8cpp.html',1,'']]],
  ['levenshtein_2eh_269',['levenshtein.h',['../levenshtein_8h.html',1,'']]],
  ['listformatitemh_270',['listFormatItemH',['../classGridCommandTable.html#a684c108764ac4837ddf84ce4041e6c2a',1,'GridCommandTable']]],
  ['listformatitemw_271',['listFormatItemW',['../classGridCommandTable.html#a0c66d38f8e0776b9c11c45613f65c82a',1,'GridCommandTable']]],
  ['loadsettings_272',['loadSettings',['../classSettings.html#a2d965ef0a054b61050811b416c896ed4',1,'Settings']]],
  ['locale_273',['locale',['../classReminderDateTable.html#ab1b3d922ab043ce5e386b06ba3db7949',1,'ReminderDateTable']]],
  ['log_274',['log',['../classImageSelectionMenu.html#acbec18a842629e24715122d57c4c8ad9',1,'ImageSelectionMenu::log()'],['../classNotesWindow.html#a83f400889db140b2dcc15d1995ecaef2',1,'NotesWindow::log()'],['../classErrorDialog.html#a0ec37a8db38d1ef1d7a586c7cd9a17ec',1,'ErrorDialog::log()'],['../classReminderWindow.html#a1177ef825775b049de9f28aabb05e22d',1,'ReminderWindow::log()']]]
];
