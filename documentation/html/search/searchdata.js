var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwx~",
  1: "acdefgimnqrstx",
  2: "acdefgilmnqrstx",
  3: "_abcdefghilmnopqrstuw~",
  4: "_acdefikmpqrstw",
  5: "cdiwx",
  6: "_abcdefgilmnoprstvw",
  7: "q",
  8: "_cqw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "properties",
  7: "related",
  8: "defines"
};

var indexSectionLabels =
{
  0: "Указатель",
  1: "Классы",
  2: "Файлы",
  3: "Функции",
  4: "Переменные",
  5: "Определения типов",
  6: "Свойства",
  7: "Друзья",
  8: "Макросы"
};

