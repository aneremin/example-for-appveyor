var searchData=
[
  ['background_5fgradient_1033',['background_gradient',['../classStylishButton.html#ad7b8cc5ab7763dabc0b2cf8a40f72b9a',1,'StylishButton']]],
  ['backgroundgrad_1034',['backgroundGrad',['../classGridCommandListItem.html#a062471b77eb8a10e29bceba971884b2f',1,'GridCommandListItem']]],
  ['backgroundgradientstart_1035',['backgroundGradientStart',['../classTheme.html#af675e3594347e48c0550c2e530e3b42a',1,'Theme']]],
  ['backgroundgradientstop_1036',['backgroundGradientStop',['../classTheme.html#a20a0879894e1826f965febc7fa92683a',1,'Theme']]],
  ['border_5fcolor_1037',['border_color',['../classGridCommandListItem.html#a2a7670f43764b8e1c60ee9688780a150',1,'GridCommandListItem::border_color()'],['../classStylishButton.html#a8b72c18c4187cc504fac0c8e74cb690a',1,'StylishButton::border_color()']]],
  ['bordercolor_1038',['borderColor',['../classTheme.html#a2c1a355894fd9bf32f03b1c8ab7ebca8',1,'Theme']]],
  ['button_5fname_1039',['button_name',['../classStylishButton.html#a22efcd1052cd8c3ab5b7d8b8586a79e0',1,'StylishButton']]]
];
