var searchData=
[
  ['settings_1003',['settings',['../classSpeechToText.html#a99b4668f6d3e3847a7fdbf288eb40312',1,'SpeechToText::settings()'],['../classSetup.html#ae2ea7b093df8866a97e497cf83fd1159',1,'Setup::settings()']]],
  ['shortcuts_1004',['shortcuts',['../classQxtGlobalShortcutPrivate.html#abe4d20108edca160ad37de258cc74ed5',1,'QxtGlobalShortcutPrivate']]],
  ['shortcutscount_1005',['shortcutsCount',['../classSpeechToText.html#a4291fb3df9288ad11b3e4ec6143422da',1,'SpeechToText']]],
  ['silentmode_1006',['silentMode',['../classSettings.html#ab73566b9c6e8045aa38e8bc3ab07ab5d',1,'Settings']]],
  ['startwindowviewed_1007',['startWindowViewed',['../classSettings.html#a6d6aa1079d0a45c0c5ce93a9cb09bfe6',1,'Settings']]],
  ['state_1008',['state',['../structXScreenSaverInfo.html#a8b78e02613ac59472df6347d6946b71c',1,'XScreenSaverInfo']]],
  ['string_1009',['string',['../classQCFString.html#ae69c066d866532465f0fd9a4f2efba8a',1,'QCFString']]],
  ['stt_1010',['stt',['../classSetup.html#ac496c7341c4aea6b0dfb1acf911ac33d',1,'Setup']]]
];
