var searchData=
[
  ['themechanged_933',['themeChanged',['../classSettings.html#af8a1e896d056c0e8b238a4ab15a92a07',1,'Settings']]],
  ['tocfstringref_934',['toCFStringRef',['../classQCFString.html#ae879ce5b64a58573af0c90cc25c013dd',1,'QCFString']]],
  ['togglerecord_935',['toggleRecord',['../classSpeechToText.html#a2ace36d39102b3737b5683e8fa1f4ef1',1,'SpeechToText']]],
  ['toint_936',['toInt',['../classDatabase.html#acdadf0de1d0860a823efa2576cab63e5',1,'Database']]],
  ['tolist_937',['toList',['../classDatabase.html#a2a950ecf74f73cc66af798479fa36814',1,'Database']]],
  ['tolower_938',['toLower',['../classDatabase.html#aef9efa4d87c1ef621e12d38c4658e690',1,'Database']]],
  ['top_5farea_5fresize_939',['top_area_resize',['../classResizeArea.html#ab0db381ac7e133ebbcf3fd29cbd99823',1,'ResizeArea']]],
  ['toqstring_940',['toQString',['../classQCFString.html#a1eac4b662da6efd2424f0494b57f41d8',1,'QCFString']]],
  ['traymodechanged_941',['trayModeChanged',['../classSettings.html#a5d3121abd6a8dd63e2078b61c26b09f8',1,'Settings']]],
  ['triggernotiftimer_942',['triggerNotifTimer',['../classMain.html#a5650e289d8eb0235d71c94a01212dd52',1,'Main']]],
  ['trimstring_943',['trimString',['../classDatabase.html#a59836fbdc34657a27ca6b8d2992930ad',1,'Database']]]
];
