var searchData=
[
  ['m_5faudiorecorder_275',['m_audioRecorder',['../classSpeechToText.html#a42a088137190323efcaf001315afdbed',1,'SpeechToText']]],
  ['m_5fmanager_276',['m_manager',['../classSpeechToText.html#ad1dd99f1ef3c7d132ae790f64df97394',1,'SpeechToText']]],
  ['main_277',['Main',['../classMain.html',1,'Main'],['../main_8cpp.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;main.cpp']]],
  ['main_2ecpp_278',['main.cpp',['../main_8cpp.html',1,'']]],
  ['main_2eqml_279',['Main.qml',['../Main_8qml.html',1,'']]],
  ['maincontextmenu_280',['MainContextMenu',['../classMainContextMenu.html',1,'']]],
  ['maincontextmenu_2eqml_281',['MainContextMenu.qml',['../MainContextMenu_8qml.html',1,'']]],
  ['makechecked_282',['makeChecked',['../classCreateCategory.html#aa49076ae91675462db579e9b195f79b3',1,'CreateCategory::makeChecked()'],['../classRightNavCategory.html#a8e37decc8d0369d3d90988dc74c535e1',1,'RightNavCategory::makeChecked()']]],
  ['makeimagemain_283',['makeImageMain',['../classDatabase.html#a547f49b66391a1412faa1f16210a7d88',1,'Database']]],
  ['menu_5fflag_284',['menu_flag',['../classMain.html#af9946821373539959633f9c1df196f81',1,'Main']]],
  ['mods_285',['mods',['../classQxtGlobalShortcutPrivate.html#a2913f92970c0e0808d3c8089ac1d170f',1,'QxtGlobalShortcutPrivate']]],
  ['mouseareaclicked_286',['mouseAreaClicked',['../classStylishButton.html#abd3910649276424296501ef277836458',1,'StylishButton']]]
];
