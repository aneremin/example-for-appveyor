QT += quick quickcontrols2 core network multimedia sql

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

include(src/classes/qxtglobalshortcut5/qxt.pri)

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        src/classes/action.cpp \
        src/classes/levenshtein.cpp \
        src/classes/database.cpp \
        src/classes/main.cpp \
        src/classes/settings.cpp \
        src/classes/speechtotext.cpp

RESOURCES += src/qrc/qml.qrc \
    src/qrc/fonts.qrc \
    src/qrc/images.qrc \
    src/qrc/sounds.qrc

RC_FILE = src/more/seticon.rc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    include/action.h \
    include/levenshtein.h \
    include/database.h \
    include/settings.h \
    include/speechtotext.h

TRANSLATIONS += translations/QtLanguage_ru.ts

OTHER_FILES += translations/QtLanguage_ru.qm
